<?php

namespace App\Http\Controllers\FrontEnd;

use App\Http\Controllers\Controller;

class GalleryController extends Controller
{
    public function index(): \Illuminate\Contracts\View\View|\Illuminate\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\Foundation\Application
    {
        return view("layout.gallery");
    }
}
