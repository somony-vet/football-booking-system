<?php

namespace App\Http\Controllers\FrontEnd;

use App\Http\Controllers\Controller;

class UserWalletController extends Controller
{
    public function index(): \Illuminate\Contracts\View\View|\Illuminate\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\Foundation\Application
    {
        return view("layout.user-wallet");
    }
}
