<?php
use App\Http\Controllers\FrontEnd\BookingController;
use App\Http\Controllers\FrontEnd\HomePageController;
use App\Http\Controllers\FrontEnd\UserDashboardController;
use App\Http\Controllers\FrontEnd\UserBookingController;
use App\Http\Controllers\FrontEnd\UserChatController;
use App\Http\Controllers\FrontEnd\UserInvoicesController;
use App\Http\Controllers\FrontEnd\UserWalletController;
use App\Http\Controllers\FrontEnd\UserProfileController;
use App\Http\Controllers\FrontEnd\UserOrtherProfileController;
use App\Http\Controllers\FrontEnd\AboutUsController;
use App\Http\Controllers\FrontEnd\ServiceController;
use App\Http\Controllers\FrontEnd\GalleryController;
use App\Http\Controllers\FrontEnd\BlogListController;
use App\Http\Controllers\FrontEnd\ContactUsController;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route ::get('/',[HomePageController::class,'index'])->name('home');
Route ::get('/home',[HomePageController::class,'index'])->name('home');

Route ::get('/user-dashboard',[UserDashboardController::class,'index'])->name('user-dashboard');

Route ::get('/cage-details',[BookingController::class,'index'])->name('cage-details');

Route ::get('/user-bookings',[UserBookingController::class,'index'])->name('user-bookings');

Route ::get('/user-chat',[UserChatController::class,'index'])->name('user-chat');

Route ::get('/user-invoice',[UserInvoicesController::class,'index'])->name('user-invoice');

Route ::get('/user-wallet',[UserWalletController::class,'index'])->name('user-wallet');

Route ::get('/user-profile',[UserProfileController::class,'index'])->name('user-profile');

Route ::get('/user-profile-othersetting',[UserOrtherProfileController::class,'index'])->name('user-profile-othersetting');

Route ::get('/about-us',[AboutUsController::class,'index'])->name('about-us');

Route ::get('/services',[ServiceController::class,'index'])->name('services');

Route ::get('/gallery',[GalleryController::class,'index'])->name('gallery');

Route ::get('/blog-list',[BlogListController::class,'index'])->name('blog-list');

Route ::get('/contact-us',[ContactUsController::class,'index'])->name('contact-us');

