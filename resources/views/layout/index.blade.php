@extends('master')
@section('title')
MN Sports
@endsection
@section('body')
<!-- Hero Section -->
<section class="hero-section" style="background: rgb(23, 124, 130);">
    <div class="banner-cock-one">
        <img src="assets/img/icons/banner-cock1.svg" alt="Banner" />
    </div>
    <div class="banner-shapes">
        <div class="banner-dot-one">
            <span></span>
        </div>
        <div class="banner-cock-two">
            <img src="assets/img/icons/banner-cock2.svg" alt="Banner" />

        </div>
        <div class="banner-dot-two">
            <span></span>
        </div>
    </div>
    <div class="container">
        <div class="home-banner" >
            <div class="row align-items-center w-100">
                <div class="col-lg-7 col-md-10 mx-auto">
                    <div class="section-search aos" data-aos="fade-up">
                        <h4 style="color: #AAF40C;">Welcome to my MN sport for booking</h4>
                        <h1>Choose Your Stadium and Start Your Training
                        </h1>
                    </div>
                </div>
                <div class="col-lg-5">
                    <div class="banner-imgs text-center aos" data-aos="fade-up">
                        <img
                            class="img-fluid img-thumbnail"
                            src="/assets/img/bg/benner-right.png"
                            alt="Banner"
                        />
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Rental Deals -->
<section class="section featured-venues">
    <div class="container">
        <div class="section-heading aos" data-aos="fade-up">
            <h2>Location for <span>Booking</span></h2>

        </div>
        <div class="row">
            <div class="featured-slider-group">
                <div class="owl-carousel featured-venues-slider owl-theme">
                    <!-- Featured Item -->
                    <div class="featured-venues-item aos" data-aos="fade-up">
                        <div class="listing-item mb-0">
                            <div class="listing-img">
                                <a href="cage-details">
                                    <img
                                        src="assets/img/venues/venues-02.jpg"
                                        class="img-fluid"
                                        alt="Venue"
                                    />
                                </a>
                                <div class="fav-item-venues">
                                    <!--                        <span class="tag tag-blue">Top Rated</span>-->
                                    <h5 class="tag tag-primary">$40<span>/hr</span></h5>
                                </div>
                            </div>
                            <div class="listing-content">
                                <!--                      <div class="list-reviews">-->
                                <!--                        <div class="d-flex align-items-center">-->
                                <!--                          <span class="rating-bg">5.0</span-->
                                <!--                          ><span>150 Reviews</span>-->
                                <!--                        </div>-->
                                <!--                        <a href="javascript:void(0)" class="fav-icon">-->
                                <!--                          <i class="feather-heart"></i>-->
                                <!--                        </a>-->
                                <!--                      </div>-->
                                <h3 class="listing-title">
                                    <a href="cage-details">PHNOM PENH CROWN FOOTBALL CLUB</a>
                                </h3>
                                <div class="listing-details-group">
                                    <p>
                                        <!--                          Unleash your badminton potential at our premier-->
                                        <!--                          Badminton Academy, where champions are made.-->
                                    </p>
                                    <ul>
                                        <li>
                            <span>
                              <i class="feather-map-pin"></i>3:00PM - 5:00PM
                            </span>
                                        </li>
                                        <li>
                            <span>
                              <i class="feather-map-pin"></i>5:00PM - 7:00PM
                            </span>
                                        </li>
                                        <li>
                            <span>
                              <i class="feather-map-pin"></i>7:00PM - 9:00PM
                            </span>
                                        </li>
                                    </ul>
                                </div>
                                <div class="listing-button">
                                    <!--                        <div class="listing-venue-owner">-->
                                    <!--                          <a class="navigation" href="coach-detail.html">-->
                                    <!--                            <img-->
                                    <!--                              src="assets/img/profiles/avatar-02.jpg"-->
                                    <!--                              alt="Venue"-->
                                    <!--                            />Rebecca-->
                                    <!--                          </a>-->
                                    <!--                        </div>-->
                                    <a href="cage-details" class="user-book-now"
                                    ><span><i class="feather-calendar me-2"></i></span
                                        >Book Now</a
                                    >
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /Featured Item -->
                    <div class="featured-venues-item aos" data-aos="fade-up">
                        <div class="listing-item mb-0">
                            <div class="listing-img">
                                <a href="cage-details">
                                    <img
                                        src="assets/img/venues/venues-01.jpg"
                                        class="img-fluid"
                                        alt="Venue"
                                    />
                                </a>
                                <div class="fav-item-venues">
                                    <!--                        <span class="tag tag-blue">Top Rated</span>-->
                                    <h5 class="tag tag-primary">$35<span>/hr</span></h5>
                                </div>
                            </div>
                            <div class="listing-content">
                                <!--                      <div class="list-reviews">-->
                                <!--                        <div class="d-flex align-items-center">-->
                                <!--                          <span class="rating-bg">5.0</span-->
                                <!--                          ><span>150 Reviews</span>-->
                                <!--                        </div>-->
                                <!--                        <a href="javascript:void(0)" class="fav-icon">-->
                                <!--                          <i class="feather-heart"></i>-->
                                <!--                        </a>-->
                                <!--                      </div>-->
                                <h3 class="listing-title">
                                    <a href="cage-details">REAL SOCCER</a>
                                </h3>
                                <div class="listing-details-group">
                                    <p>
                                        <!--                          Unleash your badminton potential at our premier-->
                                        <!--                          Badminton Academy, where champions are made.-->
                                    </p>
                                    <ul>
                                        <li>
                            <span>
                              <i class="feather-map-pin"></i>3:00PM - 5:00PM
                            </span>
                                        </li>
                                        <li>
                            <span>
                              <i class="feather-map-pin"></i>5:00PM - 7:00PM
                            </span>
                                        </li>
                                        <li>
                            <span>
                              <i class="feather-map-pin"></i>7:00PM - 9:00PM
                            </span>
                                        </li>
                                    </ul>
                                </div>
                                <div class="listing-button">
                                    <!--                        <div class="listing-venue-owner">-->
                                    <!--                          <a class="navigation" href="coach-detail.html">-->
                                    <!--                            <img-->
                                    <!--                              src="assets/img/profiles/avatar-02.jpg"-->
                                    <!--                              alt="Venue"-->
                                    <!--                            />Rebecca-->
                                    <!--                          </a>-->
                                    <!--                        </div>-->
                                    <a href="cage-details" class="user-book-now"
                                    ><span><i class="feather-calendar me-2"></i></span
                                        >Book Now</a
                                    >
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /Featured Item -->
                    <div class="featured-venues-item aos" data-aos="fade-up">
                        <div class="listing-item mb-0">
                            <div class="listing-img">
                                <a href="cage-details">
                                    <img
                                        src="assets/img/venues/venues-03.jpg"
                                        class="img-fluid"
                                        alt="Venue"
                                    />
                                </a>
                                <div class="fav-item-venues">
                                    <!--                        <span class="tag tag-blue">Top Rated</span>-->
                                    <h5 class="tag tag-primary">$50<span>/hr</span></h5>
                                </div>
                            </div>
                            <div class="listing-content">
                                <!--                      <div class="list-reviews">-->
                                <!--                        <div class="d-flex align-items-center">-->
                                <!--                          <span class="rating-bg">5.0</span-->
                                <!--                          ><span>150 Reviews</span>-->
                                <!--                        </div>-->
                                <!--                        <a href="javascript:void(0)" class="fav-icon">-->
                                <!--                          <i class="feather-heart"></i>-->
                                <!--                        </a>-->
                                <!--                      </div>-->
                                <h3 class="listing-title">
                                    <a href="cage-details">DOWN TOWN SPORT</a>
                                </h3>
                                <div class="listing-details-group">
                                    <p>
                                        <!--                          Unleash your badminton potential at our premier-->
                                        <!--                          Badminton Academy, where champions are made.-->
                                    </p>
                                    <ul>
                                        <li>
                            <span>
                              <i class="feather-map-pin"></i>3:00PM - 5:00PM
                            </span>
                                        </li>
                                        <li>
                            <span>
                              <i class="feather-map-pin"></i>5:00PM - 7:00PM
                            </span>
                                        </li>
                                        <li>
                            <span>
                              <i class="feather-map-pin"></i>7:00PM - 9:00PM
                            </span>
                                        </li>
                                    </ul>
                                </div>
                                <div class="listing-button">
                                    <!--                        <div class="listing-venue-owner">-->
                                    <!--                          <a class="navigation" href="coach-detail.html">-->
                                    <!--                            <img-->
                                    <!--                              src="assets/img/profiles/avatar-02.jpg"-->
                                    <!--                              alt="Venue"-->
                                    <!--                            />Rebecca-->
                                    <!--                          </a>-->
                                    <!--                        </div>-->
                                    <a href="cage-details" class="user-book-now"
                                    ><span><i class="feather-calendar me-2"></i></span
                                        >Book Now</a
                                    >
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Featured Item -->
                    <!--                <div class="featured-venues-item aos" data-aos="fade-up">-->
                    <!--                  <div class="listing-item mb-0">-->
                    <!--                    <div class="listing-img">-->
                    <!--                      <a href="venue-details.html">-->
                    <!--                        <img-->
                    <!--                          src="assets/img/venues/venues-04.jpg"-->
                    <!--                          class="img-fluid"-->
                    <!--                          alt="Venue"-->
                    <!--                        />-->
                    <!--                      </a>-->
                    <!--                      <div class="fav-item-venues">-->
                    <!--                        <h5 class="tag tag-primary">$350<span>/hr</span></h5>-->
                    <!--                      </div>-->
                    <!--                    </div>-->
                    <!--                    <div class="listing-content">-->
                    <!--                      <div class="list-reviews">-->
                    <!--                        <div class="d-flex align-items-center">-->
                    <!--                          <span class="rating-bg">4.7</span-->
                    <!--                          ><span>120 Reviews</span>-->
                    <!--                        </div>-->
                    <!--                        <a href="javascript:void(0)" class="fav-icon">-->
                    <!--                          <i class="feather-heart"></i>-->
                    <!--                        </a>-->
                    <!--                      </div>-->
                    <!--                      <h3 class="listing-title">-->
                    <!--                        <a href="venue-details.html">Manchester Academy</a>-->
                    <!--                      </h3>-->
                    <!--                      <div class="listing-details-group">-->
                    <!--                        <p>-->
                    <!--                          Manchester Academy: Where dreams meet excellence in-->
                    <!--                          sports education and training.-->
                    <!--                        </p>-->
                    <!--                        <ul>-->
                    <!--                          <li>-->
                    <!--                            <span>-->
                    <!--                              <i class="feather-map-pin"></i>Guysville, OH-->
                    <!--                            </span>-->
                    <!--                          </li>-->
                    <!--                          <li>-->
                    <!--                            <span>-->
                    <!--                              <i class="feather-calendar"></i>Next Availablity :-->
                    <!--                              <span class="primary-text">16 May 2023</span>-->
                    <!--                            </span>-->
                    <!--                          </li>-->
                    <!--                        </ul>-->
                    <!--                      </div>-->
                    <!--                      <div class="listing-button">-->
                    <!--                        <div class="listing-venue-owner">-->
                    <!--                          <a class="navigation" href="coach-detail.html">-->
                    <!--                            <img-->
                    <!--                              src="assets/img/profiles/avatar-03.jpg"-->
                    <!--                              alt="Venue"-->
                    <!--                            />Andrew-->
                    <!--                          </a>-->
                    <!--                        </div>-->
                    <!--                        <a href="venue-details.html" class="user-book-now"-->
                    <!--                          ><span><i class="feather-calendar me-2"></i></span-->
                    <!--                          >Book Now</a-->
                    <!--                        >-->
                    <!--                      </div>-->
                    <!--                    </div>-->
                    <!--                  </div>-->
                    <!--                </div>-->
                    <!-- /Featured Item -->
                    <!--                <div class="featured-venues-item aos" data-aos="fade-up">-->
                    <!--                  <div class="listing-item mb-0">-->
                    <!--                    <div class="listing-img">-->
                    <!--                      <a href="venue-details.html">-->
                    <!--                        <img-->
                    <!--                          src="assets/img/venues/venues-02.jpg"-->
                    <!--                          class="img-fluid"-->
                    <!--                          alt="Venue"-->
                    <!--                        />-->
                    <!--                      </a>-->
                    <!--                      <div class="fav-item-venues">-->
                    <!--                        <span class="tag tag-blue">Featured</span>-->
                    <!--                        <h5 class="tag tag-primary">$450<span>/hr</span></h5>-->
                    <!--                      </div>-->
                    <!--                    </div>-->
                    <!--                    <div class="listing-content">-->
                    <!--                      <div class="list-reviews">-->
                    <!--                        <div class="d-flex align-items-center">-->
                    <!--                          <span class="rating-bg">4.5</span-->
                    <!--                          ><span>300 Reviews</span>-->
                    <!--                        </div>-->
                    <!--                        <a href="javascript:void(0)" class="fav-icon">-->
                    <!--                          <i class="feather-heart"></i>-->
                    <!--                        </a>-->
                    <!--                      </div>-->
                    <!--                      <h3 class="listing-title">-->
                    <!--                        <a href="venue-details.html">ABC Sports Academy</a>-->
                    <!--                      </h3>-->
                    <!--                      <div class="listing-details-group">-->
                    <!--                        <p>-->
                    <!--                          Unleash your badminton potential at our premier ABC-->
                    <!--                          Sports Academy, where champions are made.-->
                    <!--                        </p>-->
                    <!--                        <ul>-->
                    <!--                          <li>-->
                    <!--                            <span>-->
                    <!--                              <i class="feather-map-pin"></i>Little Rock, AR-->
                    <!--                            </span>-->
                    <!--                          </li>-->
                    <!--                          <li>-->
                    <!--                            <span>-->
                    <!--                              <i class="feather-calendar"></i>Next Availablity :-->
                    <!--                              <span class="primary-text">17 May 2023</span>-->
                    <!--                            </span>-->
                    <!--                          </li>-->
                    <!--                        </ul>-->
                    <!--                      </div>-->
                    <!--                      <div class="listing-button">-->
                    <!--                        <div class="listing-venue-owner">-->
                    <!--                          <a class="navigation" href="coach-detail.html">-->
                    <!--                            <img-->
                    <!--                              src="assets/img/profiles/avatar-04.jpg"-->
                    <!--                              alt="Venue"-->
                    <!--                            />Mart Sublin-->
                    <!--                          </a>-->
                    <!--                        </div>-->
                    <!--                        <a href="venue-details.html" class="user-book-now"-->
                    <!--                          ><span><i class="feather-calendar me-2"></i></span-->
                    <!--                          >Book Now</a-->
                    <!--                        >-->
                    <!--                      </div>-->
                    <!--                    </div>-->
                    <!--                  </div>-->
                    <!--                </div>-->
                    <!-- /Featured Item -->
                </div>
            </div>
        </div>

        {{-- card 2 --}}
        <div class="row">
            <div class="featured-slider-group">
                <div class="owl-carousel featured-venues-slider owl-theme">
                    <!-- Featured Item -->
                    <div class="featured-venues-item aos" data-aos="fade-up">
                        <div class="listing-item mb-0">
                            <div class="listing-img">
                                <a href="cage-details">
                                    <img
                                        src="assets/img/venues/venues-04.jpg"
                                        class="img-fluid"
                                        alt="Venue"
                                    />
                                </a>
                                <div class="fav-item-venues">
                                    <!--                        <span class="tag tag-blue">Top Rated</span>-->
                                    <h5 class="tag tag-primary">$40<span>/hr</span></h5>
                                </div>
                            </div>
                            <div class="listing-content">
                                <!--                      <div class="list-reviews">-->
                                <!--                        <div class="d-flex align-items-center">-->
                                <!--                          <span class="rating-bg">5.0</span-->
                                <!--                          ><span>150 Reviews</span>-->
                                <!--                        </div>-->
                                <!--                        <a href="javascript:void(0)" class="fav-icon">-->
                                <!--                          <i class="feather-heart"></i>-->
                                <!--                        </a>-->
                                <!--                      </div>-->
                                <h3 class="listing-title">
                                    <a href="cage-details">Ministry Of National Defense Football Club</a>
                                </h3>
                                <div class="listing-details-group">
                                    <p>
                                        <!--                          Unleash your badminton potential at our premier-->
                                        <!--                          Badminton Academy, where champions are made.-->
                                    </p>
                                    <ul>
                                        <li>
                            <span>
                              <i class="feather-map-pin"></i>3:00PM - 5:00PM
                            </span>
                                        </li>
                                        <li>
                            <span>
                              <i class="feather-map-pin"></i>5:00PM - 7:00PM
                            </span>
                                        </li>
                                        <li>
                            <span>
                              <i class="feather-map-pin"></i>7:00PM - 9:00PM
                            </span>
                                        </li>
                                    </ul>
                                </div>
                                <div class="listing-button">
                                    <!--                        <div class="listing-venue-owner">-->
                                    <!--                          <a class="navigation" href="coach-detail.html">-->
                                    <!--                            <img-->
                                    <!--                              src="assets/img/profiles/avatar-02.jpg"-->
                                    <!--                              alt="Venue"-->
                                    <!--                            />Rebecca-->
                                    <!--                          </a>-->
                                    <!--                        </div>-->
                                    <a href="cage-details" class="user-book-now"
                                    ><span><i class="feather-calendar me-2"></i></span
                                        >Book Now</a
                                    >
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /Featured Item -->
                    <div class="featured-venues-item aos" data-aos="fade-up">
                        <div class="listing-item mb-0">
                            <div class="listing-img">
                                <a href="cage-details">
                                    <img
                                        src="assets/img/venues/venues-05.jpg"
                                        class="img-fluid"
                                        alt="Venue"
                                    />
                                </a>
                                <div class="fav-item-venues">
                                    <!--                        <span class="tag tag-blue">Top Rated</span>-->
                                    <h5 class="tag tag-primary">$40<span>/hr</span></h5>
                                </div>
                            </div>
                            <div class="listing-content">
                                <!--                      <div class="list-reviews">-->
                                <!--                        <div class="d-flex align-items-center">-->
                                <!--                          <span class="rating-bg">5.0</span-->
                                <!--                          ><span>150 Reviews</span>-->
                                <!--                        </div>-->
                                <!--                        <a href="javascript:void(0)" class="fav-icon">-->
                                <!--                          <i class="feather-heart"></i>-->
                                <!--                        </a>-->
                                <!--                      </div>-->
                                <h3 class="listing-title">
                                    <a href="cage-details">Platinum Sports Club </a>
                                </h3>
                                <div class="listing-details-group">
                                    <p>
                                        <!--                          Unleash your badminton potential at our premier-->
                                        <!--                          Badminton Academy, where champions are made.-->
                                    </p>
                                    <ul>
                                        <li>
                            <span>
                              <i class="feather-map-pin"></i>3:00PM - 5:00PM
                            </span>
                                        </li>
                                        <li>
                            <span>
                              <i class="feather-map-pin"></i>5:00PM - 7:00PM
                            </span>
                                        </li>
                                        <li>
                            <span>
                              <i class="feather-map-pin"></i>7:00PM - 9:00PM
                            </span>
                                        </li>
                                    </ul>
                                </div>
                                <div class="listing-button">
                                    <!--                        <div class="listing-venue-owner">-->
                                    <!--                          <a class="navigation" href="coach-detail.html">-->
                                    <!--                            <img-->
                                    <!--                              src="assets/img/profiles/avatar-02.jpg"-->
                                    <!--                              alt="Venue"-->
                                    <!--                            />Rebecca-->
                                    <!--                          </a>-->
                                    <!--                        </div>-->
                                    <a href="cage-details" class="user-book-now"
                                    ><span><i class="feather-calendar me-2"></i></span
                                        >Book Now</a
                                    >
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /Featured Item -->
                    <div class="featured-venues-item aos" data-aos="fade-up">
                        <div class="listing-item mb-0">
                            <div class="listing-img">
                                <a href="cage-details">
                                    <img
                                        src="assets/img/venues/venues-06.jpg"
                                        class="img-fluid"
                                        alt="Venue"
                                    />
                                </a>
                                <div class="fav-item-venues">
                                    <!--                        <span class="tag tag-blue">Top Rated</span>-->
                                    <h5 class="tag tag-primary">$40<span>/hr</span></h5>
                                </div>
                            </div>
                            <div class="listing-content">
                                <!--                      <div class="list-reviews">-->
                                <!--                        <div class="d-flex align-items-center">-->
                                <!--                          <span class="rating-bg">5.0</span-->
                                <!--                          ><span>150 Reviews</span>-->
                                <!--                        </div>-->
                                <!--                        <a href="javascript:void(0)" class="fav-icon">-->
                                <!--                          <i class="feather-heart"></i>-->
                                <!--                        </a>-->
                                <!--                      </div>-->
                                <h3 class="listing-title">
                                    <a href="cage-details">V SPORT Football Club </a>
                                </h3>
                                <div class="listing-details-group">
                                    <p>
                                        <!--                          Unleash your badminton potential at our premier-->
                                        <!--                          Badminton Academy, where champions are made.-->
                                    </p>
                                    <ul>
                                        <li>
                            <span>
                              <i class="feather-map-pin"></i>3:00PM - 5:00PM
                            </span>
                                        </li>
                                        <li>
                            <span>
                              <i class="feather-map-pin"></i>5:00PM - 7:00PM
                            </span>
                                        </li>
                                        <li>
                            <span>
                              <i class="feather-map-pin"></i>7:00PM - 9:00PM
                            </span>
                                        </li>
                                    </ul>
                                </div>
                                <div class="listing-button">
                                    <!--                        <div class="listing-venue-owner">-->
                                    <!--                          <a class="navigation" href="coach-detail.html">-->
                                    <!--                            <img-->
                                    <!--                              src="assets/img/profiles/avatar-02.jpg"-->
                                    <!--                              alt="Venue"-->
                                    <!--                            />Rebecca-->
                                    <!--                          </a>-->
                                    <!--                        </div>-->
                                    <a href="cage-details" class="user-book-now"
                                    ><span><i class="feather-calendar me-2"></i></span
                                        >Book Now</a
                                    >
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Featured Item -->
                    <!--                        <div class="featured-venues-item aos" data-aos="fade-up">-->
                    <!--                            <div class="listing-item mb-0">-->
                    <!--                                <div class="listing-img">-->
                    <!--                                    <a href="venue-details.html">-->
                    <!--                                        <img-->
                    <!--                                            src="assets/img/venues/venues-03.jpg"-->
                    <!--                                            class="img-fluid"-->
                    <!--                                            alt="Venue"-->
                    <!--                                        />-->
                    <!--                                    </a>-->
                    <!--                                    <div class="fav-item-venues">-->
                    <!--                                        <h5 class="tag tag-primary">$350<span>/hr</span></h5>-->
                    <!--                                    </div>-->
                    <!--                                </div>-->
                    <!--                                <div class="listing-content">-->
                    <!--                                    <div class="list-reviews">-->
                    <!--                                        <div class="d-flex align-items-center">-->
                    <!--                          <span class="rating-bg">4.7</span-->
                    <!--                          ><span>120 Reviews</span>-->
                    <!--                                        </div>-->
                    <!--                                        <a href="javascript:void(0)" class="fav-icon">-->
                    <!--                                            <i class="feather-heart"></i>-->
                    <!--                                        </a>-->
                    <!--                                    </div>-->
                    <!--                                    <h3 class="listing-title">-->
                    <!--                                        <a href="venue-details.html">Manchester Academy</a>-->
                    <!--                                    </h3>-->
                    <!--                                    <div class="listing-details-group">-->
                    <!--                                        <p>-->
                    <!--                                            Manchester Academy: Where dreams meet excellence in-->
                    <!--                                            sports education and training.-->
                    <!--                                        </p>-->
                    <!--                                        <ul>-->
                    <!--                                            <li>-->
                    <!--                            <span>-->
                    <!--                              <i class="feather-map-pin"></i>Guysville, OH-->
                    <!--                            </span>-->
                    <!--                                            </li>-->
                    <!--                                            <li>-->
                    <!--                            <span>-->
                    <!--                              <i class="feather-calendar"></i>Next Availablity :-->
                    <!--                              <span class="primary-text">16 May 2023</span>-->
                    <!--                            </span>-->
                    <!--                                            </li>-->
                    <!--                                        </ul>-->
                    <!--                                    </div>-->
                    <!--                                    <div class="listing-button">-->
                    <!--                                        <div class="listing-venue-owner">-->
                    <!--                                            <a class="navigation" href="coach-detail.html">-->
                    <!--                                                <img-->
                    <!--                                                    src="assets/img/profiles/avatar-03.jpg"-->
                    <!--                                                    alt="Venue"-->
                    <!--                                                />Andrew-->
                    <!--                                            </a>-->
                    <!--                                        </div>-->
                    <!--                                        <a href="venue-details.html" class="user-book-now"-->
                    <!--                                        ><span><i class="feather-calendar me-2"></i></span-->
                    <!--                                            >Book Now</a-->
                    <!--                                        >-->
                    <!--                                    </div>-->
                    <!--                                </div>-->
                    <!--                            </div>-->
                    <!--                        </div>-->
                    <!-- /Featured Item -->

                    <!-- Featured Item -->
                    <!--                        <div class="featured-venues-item aos" data-aos="fade-up">-->
                    <!--                            <div class="listing-item mb-0">-->
                    <!--                                <div class="listing-img">-->
                    <!--                                    <a href="venue-details.html">-->
                    <!--                                        <img-->
                    <!--                                            src="assets/img/venues/venues-02.jpg"-->
                    <!--                                            class="img-fluid"-->
                    <!--                                            alt="Venue"-->
                    <!--                                        />-->
                    <!--                                    </a>-->
                    <!--                                    <div class="fav-item-venues">-->
                    <!--                                        <span class="tag tag-blue">Featured</span>-->
                    <!--                                        <h5 class="tag tag-primary">$450<span>/hr</span></h5>-->
                    <!--                                    </div>-->
                    <!--                                </div>-->
                    <!--                                <div class="listing-content">-->
                    <!--                                    <div class="list-reviews">-->
                    <!--                                        <div class="d-flex align-items-center">-->
                    <!--                          <span class="rating-bg">4.5</span-->
                    <!--                          ><span>300 Reviews</span>-->
                    <!--                                        </div>-->
                    <!--                                        <a href="javascript:void(0)" class="fav-icon">-->
                    <!--                                            <i class="feather-heart"></i>-->
                    <!--                                        </a>-->
                    <!--                                    </div>-->
                    <!--                                    <h3 class="listing-title">-->
                    <!--                                        <a href="venue-details.html">ABC Sports Academy</a>-->
                    <!--                                    </h3>-->
                    <!--                                    <div class="listing-details-group">-->
                    <!--                                        <p>-->
                    <!--                                            Unleash your badminton potential at our premier ABC-->
                    <!--                                            Sports Academy, where champions are made.-->
                    <!--                                        </p>-->
                    <!--                                        <ul>-->
                    <!--                                            <li>-->
                    <!--                            <span>-->
                    <!--                              <i class="feather-map-pin"></i>Little Rock, AR-->
                    <!--                            </span>-->
                    <!--                                            </li>-->
                    <!--                                            <li>-->
                    <!--                            <span>-->
                    <!--                              <i class="feather-calendar"></i>Next Availablity :-->
                    <!--                              <span class="primary-text">17 May 2023</span>-->
                    <!--                            </span>-->
                    <!--                                            </li>-->
                    <!--                                        </ul>-->
                    <!--                                    </div>-->
                    <!--                                    <div class="listing-button">-->
                    <!--                                        <div class="listing-venue-owner">-->
                    <!--                                            <a class="navigation" href="coach-detail.html">-->
                    <!--                                                <img-->
                    <!--                                                    src="assets/img/profiles/avatar-04.jpg"-->
                    <!--                                                    alt="Venue"-->
                    <!--                                                />Mart Sublin-->
                    <!--                                            </a>-->
                    <!--                                        </div>-->
                    <!--                                        <a href="venue-details.html" class="user-book-now"-->
                    <!--                                        ><span><i class="feather-calendar me-2"></i></span-->
                    <!--                                            >Book Now</a-->
                    <!--                                        >-->
                    <!--                                    </div>-->
                    <!--                                </div>-->
                    <!--                            </div>-->
                    <!--                        </div>-->
                    <!-- /Featured Item -->
                </div>
            </div>
        </div>

        <!-- View More -->
        <!-- View More -->
    </div>
</section>
<!-- /Rental Deals -->
<!-- Newsletter -->
<section class="section newsletter-sport">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="subscribe-style aos" data-aos="fade-up">
                    <div class="banner-blk">
                        <img
                            src="assets/img/subscribe-bg.jpg"
                            class="img-fluid"
                            alt="Subscribe"
                        />
                    </div>
                    <div class="banner-info">
                        <img
                            src="assets/img/icons/subscribe.svg"
                            class="img-fluid"
                            alt="Subscribe"
                        />
                        <h2>Subscribe to Newsletter</h2>
                        <p>Just for you, exciting badminton news updates.</p>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- /Hero Section -->
@endsection
