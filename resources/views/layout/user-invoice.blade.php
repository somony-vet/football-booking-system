
@extends('master')
@section('title')
MN Sports
@endsection
@section('body')
		<!-- Breadcrumb -->
		<section class="breadcrumb breadcrumb-list mb-0">
			<span class="primary-right-round"></span>
			<div class="container mt-lg-3">
				<h1 class="text-white mt-lg-5">Invoice</h1>
				<ul>
					<li><a href="home">Home</a></li>
					<li >Invoice</li>
				</ul>
			</div>
		</section>
		<!-- /Breadcrumb -->

		<!-- Dashboard Menu -->
		<div class="dashboard-section">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="dashboard-menu">
							<ul>
								<li>
									<a href="user-dashboard">
										<img src="assets/img/icons/dashboard-icon.svg" alt="Icon">
										<span>Dashboard</span>
									</a>
								</li>
								<li>
									<a href="user-bookings" >
										<img src="assets/img/icons/booking-icon.svg" alt="Icon">
										<span>My Bookings</span>
									</a>
								</li>
								<li>
									<a href="user-chat">
										<img src="assets/img/icons/chat-icon.svg" alt="Icon">
										<span>Chat</span>
									</a>
								</li>
								<li>
									<a href="user-invoice" class="active">
										<img src="assets/img/icons/invoice-icon.svg" alt="Icon">
										<span>Invoices</span>
									</a>
								</li>
								<li>
									<a href="user-wallet">
										<img src="assets/img/icons/wallet-icon.svg" alt="Icon">
										<span>Wallet</span>
									</a>
								</li>
								<li>
									<a href="user-profile">
										<img src="assets/img/icons/profile-icon.svg" alt="Icon">
										<span>Profile Setting</span>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- /Dashboard Menu -->

		<!-- Page Content -->
		<div class="content court-bg">
			<div class="container">

				<!-- Sort By -->
				<!-- Sort By -->

				<div class="row">
					<div class="col-sm-12">
						<div class="court-tab-content">
							<div class="card card-tableset">
								<div class="card-body">
									<div class="coache-head-blk">
										<div class="row align-items-center">
											<div class="col-md-5">
												<div class="court-table-head">
													<h4>Invoices</h4>
													<p>Access recent invoices related to court bookings</p>
												</div>
											</div>
											<div class="col-md-7">
												<div class="table-search-top invoice-search-top">
													<div id="tablefilter"></div>
													<div class="request-coach-list select-filter">
														<div class="sortby-filter-group court-sortby">
															<div class="sortbyset m-0">
																<div class="sorting-select">
																	<select class="form-control select">
																		<option>All Invoices</option>
																		<option>Completed</option>
																		<option>Inprogress</option>
																	</select>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="table-responsive table-datatable">
                                        <table class="table datatable">
											<thead class="thead-light">
                                            <tr>
                                                    <th>ID</th>
												   <th>Court Name</th>
												   <th>Date & Time  </th>
												   <th>Payment</th>
												   <th>Paid On</th>
												   <th>Status</th>
												   <th>Download</th>
												   <th></th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td><a class="text-primary">2</a></td>
													<td>
														<h2 class="table-avatar">
															<a href="#" class="avatar avatar-sm flex-shrink-0"><img class="avatar-img" src="assets/img/venues/venues-02.jpg" alt="User Image"></a>
															<span class="table-head-name flex-grow-1">
																<a href="#">DOWNTOWN SPORT</a>
															</span>
														</h2>
													</td>
													<td class="table-date-time">
														<h4>Mon, Jul 12<span>06:00 PM - 08:00 PM</span></h4>
													</td>
													<td><span class="pay-dark fs-16">50$</span></td>
													<td>Mon, Jul 12</td>
													<td><span class="badge bg-success"><i class="feather-check-square me-1"></i>Paid</span></td>
													<td class="text-pink view-detail-pink"><a href="javascript:;"><i class="feather-download"></i>Download</a></td>
													<td class="text-end">
														<div class="dropdown dropdown-action table-drop-action">
															<a href="#" class="action-icon dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false"><i class="fas fa-ellipsis-h"></i></a>
															<div class="dropdown-menu dropdown-menu-end">
																<a class="dropdown-item" href="javascript:void(0);"><i class="feather-briefcase"></i>Refund</a>
																<a class="dropdown-item" href="javascript:void(0);"><i class="feather-trash"></i>Delete</a>
															</div>
														</div>
													</td>
												</tr>
												<tr>
													<td><a  class="text-primary">3</a></td>
													<td>
														<h2 class="table-avatar">
															<a href="#" class="avatar avatar-sm flex-shrink-0"><img class="avatar-img" src="assets/img/venues/venues-03.jpg" alt="User"></a>
															<span class="table-head-name flex-grow-1">
																<a href="#">Ministry Of National Defense Football Club</a>
															</span>
														</h2>
													</td>
													<td class="table-date-time">
														<h4>Mon, Jul 13<span>5:00 PM - 7:00 PM</span></h4>
													</td>
													<td><span class="pay-dark fs-16">40</span></td>
													<td>Mon, Jul 13</td>
													<td><span class="badge bg-info"><i class="feather-check-square me-1"></i>Pending</span></td>
													<td class="text-pink view-detail-pink"><a href="javascript:;"><i class="feather-download"></i>Download</a></td>
													<td class="text-end">
														<div class="dropdown dropdown-action table-drop-action">
															<a href="#" class="action-icon dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false"><i class="fas fa-ellipsis-h"></i></a>
															<div class="dropdown-menu dropdown-menu-end">
																<a class="dropdown-item" href="javascript:void(0);"><i class="feather-briefcase"></i>Refund</a>
																<a class="dropdown-item" href="javascript:void(0);"><i class="feather-trash"></i>Delete</a>
															</div>
														</div>
													</td>
												</tr>
												<tr>
													<td><a class="text-primary">1</a></td>
													<td>
														<h2 class="table-avatar">
															<a href="#" class="avatar avatar-sm flex-shrink-0"><img class="avatar-img" src="assets/img/venues/venues-01.jpg" alt="User"></a>
															<span class="table-head-name flex-grow-1">
																<a href="#">REAL SOCCER</a>

															</span>
														</h2>
													</td>
													<td class="table-date-time">
														<h4>Mon, Jul 14<span>01:00 PM - 02:00 PM</span></h4>
													</td>
													<td><span class="pay-dark fs-16">35$</span></td>
													<td>Mon, Jul 14</td>
													<td><span class="badge bg-danger"><i class="feather-check-square me-1"></i>Failed</span></td>
													<td class="text-pink view-detail-pink"><a href="javascript:;"><i class="feather-download"></i>Download</a></td>
													<td class="text-end">
														<div class="dropdown dropdown-action table-drop-action">
															<a href="#" class="action-icon dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false"><i class="fas fa-ellipsis-h"></i></a>
															<div class="dropdown-menu dropdown-menu-end">
																<a class="dropdown-item" href="javascript:void(0);"><i class="feather-briefcase"></i>Refund</a>
																<a class="dropdown-item" href="javascript:void(0);"><i class="feather-trash"></i>Delete</a>
															</div>
														</div>
													</td>
												</tr>
											</tbody>
										</table>
									</div>
                                </div>
							</div>
							<div class="tab-footer">
								<div class="row">
									<div class="col-md-6">
										<div id="tablelength"></div>
									</div>
									<div class="col-md-6">
										<div id="tablepage"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
		<!-- /Page Content -->

		<!-- Footer -->
@endsection
