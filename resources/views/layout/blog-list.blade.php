@extends('master')
@section('title')
MN Sports
@endsection
@section('body')
		<!-- Breadcrumb -->
		<div class="breadcrumb breadcrumb-list mb-0">
			<span class="primary-right-round"></span>
			<div class="container mt-lg-3">
				<h1 class="text-white mt-lg-5">Blog List</h1>
				<ul>
					<li><a href="index.html">Home</a></li>
					<li>Blog List</li>
				</ul>
			</div>
		</div>
		<!-- /Breadcrumb -->
		<!-- Page Content -->
		<div class="content blog-grid">
			<div class="container">
				<div class="row">
					<div class="col-sm-12 col-md-10 col-lg-8 mx-auto">
						<!-- Blog -->
					    <div class="featured-venues-item">
							<div class="listing-item">
								<div class="listing-img">
									<a href="blog-list">
										<img src="assets/img/blog/blog-01.jpg" class="img-fluid" alt="Venue">
									</a>
									<div class="fav-item-venues news-sports">
										<span class="tag tag-blue">SOCCER</span>
										<div class="list-reviews coche-star">
											<a href="blog-list" class="fav-icon">
												<i class="feather-heart"></i>
											</a>
										</div>
									</div>
								</div>
								<div class="listing-content news-content">
									<div class="listing-venue-owner">
										<div class="navigation">
											<a href="javascript:void(0)"><img src="assets/img/profiles/avatar-01.jpg" alt="User">Orlando Waters</a>
											<span ><i class="feather-calendar"></i>15 May 2023</span>
										</div>
									</div>
									<h3 class="listing-title">
										<a href="blog-list">Football Friendly Match </a>
									</h3>
									<p>Master the fundamentals of football with our beginner's guide, unlocking the path to skillful play and enjoyment on the field during friendly matches.</p>
									<div class="listing-button read-new">
										<ul class="nav">
											<li><a href="javascript:void(0);"><i class="feather-heart"></i>45</a></li>
											<li><a href="javascript:void(0);"><i class="feather-message-square"></i>40</a></li>
										</ul>
										<span><img src="assets/img/icons/clock.svg" alt="Icon">10 Min To Read</span>
									</div>
								</div>
							</div>
						</div>
						<!-- /Blog -->
						<!-- Blog -->

						<!-- /Blog -->
					</div>
				</div>
				<!--Pagination-->
				<div class="blog-pagination">
					<nav>
				     	<ul class="pagination justify-content-center pagination-center">
							<li class="page-item previtem">
					        	<a class="page-link" href="javascript:void(0);"><i class="feather-chevrons-left"></i></a>
							</li>
							<li class="page-item previtem">
					        	<a class="page-link" href="javascript:void(0);"><i class="feather-chevron-left"></i></a>
							</li>
							<li class="page-item">
								<a class="page-link active" href="javascript:void(0);">1</a>
							</li>
							<li class="page-item active">
								<a class="page-link" href="javascript:void(0);">2</a>
							</li>
							<li class="page-item">
								<a class="page-link" href="javascript:void(0);">3</a>
						   </li>
							<li class="page-item nextlink">
    							<a class="page-link" href="javascript:void(0);"> <i class="feather-chevron-right"></i></a>
							</li>
							<li class="page-item nextlink">
    							<a class="page-link" href="javascript:void(0);"> <i class="feather-chevrons-right"></i></a>
							</li>
						</ul>
					</nav>
 			    </div>
			    <!--Pagination-->
			</div>
		</div>
		<!-- /Page Content -->
@endsection
