@extends('master')
@section('title')
MN Sports
@endsection
@section('body')


		<!-- Breadcrumb -->
		<div><section class="breadcrumb breadcrumb-list mb-0">
			<span class="primary-right-round"></span>
			<div class="container mt-lg-3">
				<h1 class="text-white mt-lg-5">User Dashboard</h1>
				<ul>
					<li><a href="home">Home</a></li>
					<li >User Dashboard</li>
				</ul>
			</div>
		</section>
		<!-- /Breadcrumb -->

		<!-- Dashboard Menu -->
		<div class="dashboard-section">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="dashboard-menu">
							<ul>
								<li>
									<a href="user-dashboard" class="active">
										<img src="assets/img/icons/dashboard-icon.svg" alt="Icon">
										<span>Dashboard</span>
									</a>
								</li>
								<li>
									<a href="user-bookings">
										<img src="assets/img/icons/booking-icon.svg" alt="Icon">
										<span>My Bookings</span>
									</a>
								</li>
								<li>
									<a href="user-chat">
										<img src="assets/img/icons/chat-icon.svg" alt="Icon">
										<span>Chat</span>
									</a>
								</li>
								<li>
									<a href="user-invoice">
										<img src="assets/img/icons/invoice-icon.svg" alt="Icon">
										<span>Invoices</span>
									</a>
								</li>
								<li>
									<a href="user-wallet">
										<img src="assets/img/icons/wallet-icon.svg" alt="Icon">
										<span>Wallet</span>
									</a>
								</li>
								<li>
									<a href="user-profile">
										<img src="assets/img/icons/profile-icon.svg" alt="Icon">
										<span>Profile Setting</span>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- /Dashboard Menu -->

		<!-- Page Content -->
		<div class="content">
			<div class="container">

				<!-- Statistics Card -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card dashboard-card statistics-card">
                            <div class="card-header">
                                <h4>Statistics</h4>
                                <p>Boost your game with stats and goals tailored to you</p>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-xl-4 col-lg-4 col-md-4 d-flex">
                                        <div class="statistics-grid flex-fill">
                                            <div class="statistics-content">
                                                <h3>6</h3>
                                                <p>Stadium</p>
                                            </div>
                                            <div class="statistics-icon">
                                                <img src="assets/img/icons/statistics-01.svg" alt="Icon">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-4 col-lg-4 col-md-4 d-flex">
                                        <div class="statistics-grid flex-fill">
                                            <div class="statistics-content">
                                                <h3>3</h3>
                                                <p>Total Booked</p>
                                            </div>
                                            <div class="statistics-icon">
                                                <img src="assets/img/icons/statistics-02.svg" alt="Icon">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-4 col-lg-4 col-md-4 d-flex">
                                        <div class="statistics-grid flex-fill">
                                            <div class="statistics-content">
                                                <h3>$45,056</h3>
                                                <p>Payments</p>
                                            </div>
                                            <div class="statistics-icon">
                                                <img src="assets/img/icons/statistics-04.svg" alt="Icon">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
				<!-- /Statistics Card -->

				<!-- Dashboard Table -->


				<!-- /Dashboard Table -->

			</div>
		</div>
		<!-- /Page Content -->

            @endsection
