@extends('master')
@section('title')
MN Sports
@endsection
@section('body')
		<!-- Breadcrumb -->
		<section class="breadcrumb breadcrumb-list mb-0">
			<span class="primary-right-round"></span>
			<div class="container mt-lg-3">
				<h1 class="text-white">User Profile</h1>
				<ul>
					<li><a href="index.html">Home</a></li>
					<li>User Profile</li>
				</ul>
			</div>
		</section>
		<!-- /Breadcrumb -->

		<!-- Dashboard Menu -->
		<div class="dashboard-section">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="dashboard-menu">
							<ul>
								<li>
									<a href="user-dashboard.html">
										<img src="assets/img/icons/dashboard-icon.svg" alt="Icon">
										<span>Dashboard</span>
									</a>
								</li>
								<li>
									<a href="user-bookings.html" >
										<img src="assets/img/icons/booking-icon.svg" alt="Icon">
										<span>My Bookings</span>
									</a>
								</li>
								<li>
									<a href="user-chat.html">
										<img src="assets/img/icons/chat-icon.svg" alt="Icon">
										<span>Chat</span>
									</a>
								</li>
								<li>
									<a href="user-invoice.html" >
										<img src="assets/img/icons/invoice-icon.svg" alt="Icon">
										<span>Invoices</span>
									</a>
								</li>
								<li>
									<a href="user-wallet.html" >
										<img src="assets/img/icons/wallet-icon.svg" alt="Icon">
										<span>Wallet</span>
									</a>
								</li>
								<li>
									<a href="user-profile.html" class="active">
										<img src="assets/img/icons/profile-icon.svg" alt="Icon">
										<span>Profile Setting</span>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- /Dashboard Menu -->

		<!-- Page Content -->
		<div class="content court-bg">
			<div class="container">

				<div class="coach-court-list profile-court-list">
					<ul class="nav">
						<li><a  href="user-profile">Profile</a></li>
						<li><a  class="active" href="user-profile-othersetting">Other Settings</a></li>
					</ul>
				</div>

				<div class="row">
					<div class="col-sm-12">
						<div class="profile-detail-group">
							<div class="card ">
								<form >
									<div class="row">
										<div class="col-lg-12">
											<div class="appoint-head">
												<h4>Change Email</h4>
											</div>
											<div class="input-space other-setting-form">
												<label   class="form-label"> Enter New Email Address</label>
												<input type="email" class="form-control" placeholder="Enter New Email Address">
											</div>
										</div>
										<div class="col-lg-12">
											<div class="appoint-head">
												<h4>Change Phone Number</h4>
											</div>
											<div class="input-space other-setting-form">
												<label   class="form-label">New Phone Number</label>
												<input type="email" class="form-control" placeholder="Enter New  Phone Number">
											</div>
										</div>
									</div>
								</form>
							</div>
							<div class="save-changes text-end">
								<a href="javascript:;" class="btn btn-primary reset-profile">Reset</a>
								<a href="javascript:;"  class="btn btn-secondary save-profile" data-bs-toggle="modal" data-bs-target="#success-mail">Save Change</a>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
		<!-- /Page Content -->

@endsection
