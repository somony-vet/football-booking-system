@extends('master')
@section('title')
MN Sports
@endsection
@section('body')
		<!-- Breadcrumb -->
		<div class="breadcrumb breadcrumb-list mb-0">
			<span class="primary-right-round"></span>
			<div class="container mt-lg-3">
				<h1 class="text-white">Services</h1>
				<ul>
					<li><a href="home">Home</a></li>
					<li>Services</li>
				</ul>
			</div>
		</div>
		<!-- /Breadcrumb -->

		<!-- Page Content -->
		<div class="content">

			<div class="container">
				<section class="services">
					<ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
						<li class="nav-item" role="presentation">
						<button class="nav-link active" id="pills-all-tab" data-bs-toggle="pill" data-bs-target="#pills-all" type="button" role="tab" aria-controls="pills-all" aria-selected="true">Services</button>
						</li>

					</ul>
					<div class="tab-content" id="pills-tabContent">
						<div class="tab-pane fade show active" id="pills-all" role="tabpanel" aria-labelledby="pills-all-tab">
							<div class="row">
								<div class="col-12 col-sm-12 col-md-4 col-lg-4 d-flex">
									<div class="listing-item">
										<div class="listing-img">
											<a href="service">
												<img src="assets/img/services/service-05.jpg" class="img-fluid" alt="Service">
											</a>
										</div>
										<div class="listing-content text-center">
											<h3 class="listing-title">
												<a href="service-detail">Home Sport</a>
											</h3>
											<p>Depending on the sport you're interested in, you'll need specific equipment. For example, in football, you might need football boots (cleats), a football, shin guards, gloves, and protective gear. It's important to choose equipment that is of high quality, fits properly,</p>
											<a href="service" class="btn btn-secondary">Read More</a>
										</div>
									</div>
								</div>
								<div class="col-12 col-sm-12 col-md-4 col-lg-4 d-flex">
									<div class="listing-item">
										<div class="listing-img">
											<a href="service-detail.html">
												<img src="assets/img/services/service-06.jpg" class="img-fluid" alt="Service">
											</a>
										</div>
										<div class="listing-content text-center">
											<h3 class="listing-title">
												<a href="service-detail.html">Food and Energy Drink</a>
											</h3>
											<p>Athletes require a well-balanced diet that includes carbohydrates, protein, healthy fats, vitamins, and minerals. Carbohydrates are essential for providing energy, while protein supports muscle repair and growth. Incorporate a variety of fruits, vegetables, whole grains, lean proteins, and healthy fats into your meals.</p>
											<a href="service-detail.html" class="btn btn-secondary">Read More</a>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="tab-pane fade" id="pills-coaching" role="tabpanel" aria-labelledby="pills-coaching-tab">
							<div class="row">
								<div class="col-12 col-sm-12 col-md-4 col-lg-4">
									<div class="listing-item">
										<div class="listing-img">
											<a href="service-detail.html">
												<img src="assets/img/services/service-05.jpg" class="img-fluid" alt="Service">
											</a>
										</div>
										<div class="listing-content text-center">
											<h3 class="listing-title">
												<a href="service-detail.html">Sports Performance Training</a>
											</h3>
											<p>DreamSports offers tailored sports performance training programs designed to enhance your athletic performance in badminton.</p>
											<a href="service-detail.html" class="btn btn-secondary">Read More</a>
										</div>
									</div>
								</div>
								<div class="col-12 col-sm-12 col-md-4 col-lg-4">
									<div class="listing-item">
										<div class="listing-img">
											<a href="service-detail.html">
												<img src="assets/img/services/service-06.jpg" class="img-fluid" alt="Service">
											</a>
										</div>
										<div class="listing-content text-center">
											<h3 class="listing-title">
												<a href="service-detail.html">Sports Science and Technology</a>
											</h3>
											<p>Utilize advanced technology, such as video analysis and motion sensors, to evaluate and improve your technique.</p>
											<a href="service-detail.html" class="btn btn-secondary">Read More</a>
										</div>
									</div>
								</div>
								<div class="col-12 col-sm-12 col-md-4 col-lg-4">
									<div class="listing-item">
										<div class="listing-img">
											<a href="service-detail.html">
												<img src="assets/img/services/service-07.jpg" class="img-fluid" alt="Service">
											</a>
										</div>
										<div class="listing-content text-center">
											<h3 class="listing-title">
												<a href="service-detail.html">Sports Performance Training</a>
											</h3>
											<p>Discover the thrill of group lessons in badminton, where you can enhance your skills, connect with others, and enjoy the sport to the fullest.</p>
											<a href="service-detail.html" class="btn btn-secondary">Read More</a>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="tab-pane fade" id="pills-lessons" role="tabpanel" aria-labelledby="pills-lessons-tab">
							<div class="row">
								<div class="col-12 col-sm-12 col-md-4 col-lg-4">
									<div class="listing-item">
										<div class="listing-img">
											<a href="service-detail.html">
												<img src="assets/img/services/service-08.jpg" class="img-fluid" alt="Service">
											</a>
										</div>
										<div class="listing-content text-center">
											<h3 class="listing-title">
												<a href="service-detail.html">Service 4</a>
											</h3>
											<p>Lorem ipsum is simply free text dolor sit am adipi we help you ensure everyone.</p>
											<a href="service-detail.html" class="btn btn-secondary">Read More</a>
										</div>
									</div>
								</div>
								<div class="col-12 col-sm-12 col-md-4 col-lg-4">
									<div class="listing-item">
										<div class="listing-img">
											<a href="service-detail.html">
												<img src="assets/img/services/service-09.jpg" class="img-fluid" alt="Service">
											</a>
										</div>
										<div class="listing-content text-center">
											<h3 class="listing-title">
												<a href="service-detail.html">Service 5</a>
											</h3>
											<p>Lorem ipsum is simply free text dolor sit am adipi we help you ensure everyone.</p>
											<a href="service-detail.html" class="btn btn-secondary">Read More</a>
										</div>
									</div>
								</div>
								<div class="col-12 col-sm-12 col-md-4 col-lg-4">
									<div class="listing-item">
										<div class="listing-img">
											<a href="service-detail.html">
												<img src="assets/img/services/service-10.jpg" class="img-fluid" alt="Service">
											</a>
										</div>
										<div class="listing-content text-center">
											<h3 class="listing-title">
												<a href="service-detail.html">Service 6</a>
											</h3>
											<p>Lorem ipsum is simply free text dolor sit am adipi we help you ensure everyone.</p>
											<a href="service-detail.html" class="btn btn-secondary">Read More</a>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="tab-pane fade" id="pills-coaches" role="tabpanel" aria-labelledby="pills-coaches-tab">
							<div class="row">
								<div class="col-12 col-sm-12 col-md-4 col-lg-4">
									<div class="listing-item">
										<div class="listing-img">
											<a href="service-detail.html">
												<img src="assets/img/services/service-05.jpg" class="img-fluid" alt="Service">
											</a>
										</div>
										<div class="listing-content text-center">
											<h3 class="listing-title">
												<a href="service-detail.html">Elite Badminton</a>
											</h3>
											<p>Our team of experienced and qualified coaches is dedicated to helping you achieve your goals.</p>
											<a href="service-detail.html" class="btn btn-secondary">Read More</a>
										</div>
									</div>
								</div>
								<div class="col-12 col-sm-12 col-md-4 col-lg-4">
									<div class="listing-item">
										<div class="listing-img">
											<a href="service-detail.html">
												<img src="assets/img/services/service-06.jpg" class="img-fluid" alt="Service">
											</a>
										</div>
										<div class="listing-content text-center">
											<h3 class="listing-title">
												<a href="service-detail.html">Service 2</a>
											</h3>
											<p>Our team of experienced and qualified coaches is dedicated to helping you achieve your goals.</p>
											<a href="service-detail.html" class="btn btn-secondary">Read More</a>
										</div>
									</div>
								</div>
								<div class="col-12 col-sm-12 col-md-4 col-lg-4">
									<div class="listing-item">
										<div class="listing-img">
											<a href="service-detail.html">
												<img src="assets/img/services/service-07.jpg" class="img-fluid" alt="Service">
											</a>
										</div>
										<div class="listing-content text-center">
											<h3 class="listing-title">
												<a href="service-detail.html">Service 3</a>
											</h3>
											<p>Our team of experienced and qualified coaches is dedicated to helping you achieve your goals.</p>
											<a href="service-detail.html" class="btn btn-secondary">Read More</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
			</div>

		</div>
		<!-- /Page Content -->
@endsection
