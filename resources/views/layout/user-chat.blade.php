@extends('master')
@section('title')
MN Sports
@endsection
@section('body')

		<!-- Breadcrumb -->
		<section class="breadcrumb breadcrumb-list mb-0">
			<span class="primary-right-round"></span>
			<div class="container mt-lg-3">
				<h1 class="text-white mt-lg-5">Chat</h1>
				<ul>
					<li><a href="home">Home</a></li>
					<li>Chat</li>
				</ul>
			</div>
		</section>
		<!-- /Breadcrumb -->

		<!-- Dashboard Menu -->
		<div class="dashboard-section">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="dashboard-menu">
							<ul>
								<li>
									<a href="user-dashboard">
										<img src="assets/img/icons/dashboard-icon.svg" alt="Icon">
										<span>Dashboard</span>
									</a>
								</li>
								<li>
									<a href="user-bookings" >
										<img src="assets/img/icons/booking-icon.svg" alt="Icon">
										<span>My Bookings</span>
									</a>
								</li>
								<li>
									<a href="user-chat" class="active">
										<img src="assets/img/icons/chat-icon.svg" alt="Icon">
										<span>Chat</span>
									</a>
								</li>
								<li>
									<a href="user-invoice">
										<img src="assets/img/icons/invoice-icon.svg" alt="Icon">
										<span>Invoices</span>
									</a>
								</li>
								<li>
									<a href="user-wallet">
										<img src="assets/img/icons/wallet-icon.svg" alt="Icon">
										<span>Wallet</span>
									</a>
								</li>
								<li>
									<a href="user-profile">
										<img src="assets/img/icons/profile-icon.svg" alt="Icon">
										<span>Profile Setting</span>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- /Dashboard Menu -->

		<!-- Page Content -->
		<div class="content court-bg">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="chat-window">

							<!-- Chat Left -->
							<div class="chat-cont-left">
								<form class="chat-search">
									<div class="form-custom">
										<input type="text" class="form-control" placeholder="Search">
									</div>
								</form>
								<div class="chat-users-list">
									<h3>Contacts</h3>
									<div class="chat-scroll">
										<a href="javascript:void(0);" class="media">
											<div class="media-img-wrap">
												<div class="avatar avatar-online">
													<img src="assets/img/profiles/avatar-01.jpg" alt="User" class="avatar-img rounded-circle">
												</div>
											</div>
											<div class="media-body">
												<div>
													<div class="user-name">B.Messi(Goat)</div>
													<div class="user-last-chat"><i class="feather-check"></i> Hi!!!</div>
												</div>
												<div>
													<div class="last-chat-time block">2 min</div>
													<div class="badge badge-success badge-pill">15</div>
												</div>
											</div>
										</a>
										<a href="javascript:void(0);" class="media read-chat active">
											<div class="media-img-wrap">
												<div class="avatar avatar-online">
													<img src="assets/img/profiles/avatar-02.jpg" alt="User" class="avatar-img rounded-circle">
												</div>
											</div>
											<div class="media-body">
												<div>
													<div class="user-name">Ah kak Ronaldo</div>
													<div class="user-last-chat"><i class="fa-solid fa-check-double"></i> Hi!!!</div>
												</div>
												<div>
													<div class="last-chat-time block">8:01 PM</div>
												</div>
											</div>
										</a>
										<a href="javascript:void(0);" class="media">
											<div class="media-img-wrap">
												<div class="avatar avatar-online">
													<img src="assets/img/profiles/avatar-03.jpg" alt="User" class="avatar-img rounded-circle">
												</div>
											</div>
											<div class="media-body">
												<div>
													<div class="user-name">Chan Vatanaka</div>
													<div class="user-last-chat"><i class="fa-solid fa-check-double"></i> Hi!!!</div>
												</div>
												<div>
													<div class="last-chat-time block">7:30 PM</div>
													<div class="badge badge-success badge-pill">3</div>
												</div>
											</div>
										</a>
									</div>
								</div>
							</div>
							<!-- /Chat Left -->

							<!-- Chat Right -->
							<div class="chat-cont-right">
								<div class="chat-header">
									<a id="back_user_list" href="javascript:void(0)" class="back-user-list">
										<i class="feather-chevrons-left"></i>
									</a>
									<div class="media">
										<div class="media-img-wrap">
											<div class="avatar avatar-online">
												<img src="assets/img/profiles/avatar-02.jpg" alt="User" class="avatar-img rounded-circle">
											</div>
										</div>
										<div class="media-body">
											<div class="user-name">Ah Kak Ronaldo</div>
										</div>
									</div>
									<div class="chat-options">
										<div class="dropdown dropdown-action table-drop-action">
											<a href="#" class="action-icon dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false"><i class="fas fa-ellipsis-v"></i></a>
											<div class="dropdown-menu dropdown-menu-end">
												<a class="dropdown-item" href="javascript:void(0);"><i class="feather feather-archive"></i>Achive</a>
												<a class="dropdown-item" href="javascript:void(0);"><i class="feather feather-mic-off"></i>Muted</a>
												<a class="dropdown-item" href="javascript:void(0);"><i class="feather feather-trash"></i>Delete</a>
											</div>
										</div>
									</div>
								</div>
								<div class="chat-body">
									<div class="chat-scroll">
										<ul class="list-unstyled">
											<li class="media sent read-chat">
												<div class="media-body">
													<div class="msg-box">
														<div>
															<p>I Just Booked you for a single lesson </p>
															<ul class="chat-msg-info">
																<li>
																	<div class="chat-time">
																		<span>8:30 AM</span>
																		<span class="msg-seen"><i class="fa-solid fa-check-double"></i></span>
																	</div>
																</li>
															</ul>
														</div>
													</div>
												</div>
												<div class="avatar">
													<img src="assets/img/profiles/avatar-02.jpg" alt="User" class="avatar-img rounded-circle">
												</div>
											</li>
											<li class="media received">
												<div class="avatar">
													<img src="assets/img/profiles/avatar-03.jpg" alt="User" class="avatar-img rounded-circle">
												</div>
												<div class="media-body">
													<div class="msg-box">
														<div>
															<p>I Just Booked you for a single lesson ?</p>
															<p>Ok?</p>
															<ul class="chat-msg-info">
																<li>
																	<div class="chat-time">
																		<span>8:30 AM</span>
																		<span class="msg-seen"><i class="fa-solid fa-check-double"></i></span>
																	</div>
																</li>
															</ul>
														</div>
													</div>
												</div>
											</li>
											<li class="chat-date">Today</li>
											<li class="media received">
												<div class="avatar">
													<img src="assets/img/profiles/avatar-03.jpg" alt="User" class="avatar-img rounded-circle">
												</div>
												<div class="media-body">
													<div class="msg-box">
														<div>
															<p>Can you please Come with Players on same day??</p>
															<ul class="chat-msg-info">
																<li>
																	<div class="chat-time">
																		<span>8:30 AM</span>
																		<span class="msg-seen"><i class="fa-solid fa-check-double"></i></span>
																	</div>
																</li>
															</ul>
														</div>
													</div>
												</div>
											</li>
											<li class="media sent">
												<div class="media-body">
													<div class="msg-box">
														<div>
															<p>Okay Thanks you!! Appreciated</p>
															<div class="chat-msg-actions dropdown">
																<a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
																	<i class="fe fe-elipsis-v"></i>
																</a>
																<div class="dropdown-menu dropdown-menu-right">
																	<a class="dropdown-item" href="#">Delete</a>
																</div>
															</div>
															<ul class="chat-msg-info">
																<li>
																	<div class="chat-time">
																		<span>8:30 AM</span>
																		<span class="msg-seen"><i class="fa-solid fa-check-double"></i></span>
																	</div>
																</li>
															</ul>
														</div>
													</div>
												</div>
												<div class="avatar">
													<img src="assets/img/profiles/avatar-02.jpg" alt="User" class="avatar-img rounded-circle">
												</div>
											</li>
										</ul>
									</div>
								</div>
								<div class="chat-footer">
									<div class="form-custom">
										<div class="input-group-prepend">
											<i class="feather-paperclip"></i>
										</div>
										<div class="send-blk">
											<input type="text" class="input-msg-send form-control" placeholder="Type something">
											<div class="input-group-append">
												<button type="button" class="btn msg-send-btn"><i class="feather-send"></i></button>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- /Chat Right -->

						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- /Page Content -->

@endsection
