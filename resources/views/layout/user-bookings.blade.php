@extends('master')
@section('title')
MN Sports
@endsection
@section('body')

		<!-- Breadcrumb -->
		<section class="breadcrumb breadcrumb-list mb-0">
			<span class="primary-right-round"></span>
			<div class="container mt-lg-3">
				<h1 class="text-white mt-lg-5">User Bookings</h1>
				<ul>
					<li><a href="home">Home</a></li>
					<li>User Bookings</li>
				</ul>
			</div>
		</section>
		<!-- /Breadcrumb -->

		<!-- Dashboard Menu -->
		<div class="dashboard-section">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="dashboard-menu">
							<ul>
								<li>
									<a href="user-dashboard">
										<img src="assets/img/icons/dashboard-icon.svg" alt="Icon">
										<span>Dashboard</span>
									</a>
								</li>
								<li>
									<a href="user-bookings" class="active">
										<img src="assets/img/icons/booking-icon.svg" alt="Icon">
										<span>My Bookings</span>
									</a>
								</li>
								<li>
									<a href="user-chat">
										<img src="assets/img/icons/chat-icon.svg" alt="Icon">
										<span>Chat</span>
									</a>
								</li>
								<li>
									<a href="user-invoice">
										<img src="assets/img/icons/invoice-icon.svg" alt="Icon">
										<span>Invoices</span>
									</a>
								</li>
								<li>
									<a href="user-wallet">
										<img src="assets/img/icons/wallet-icon.svg" alt="Icon">
										<span>Wallet</span>
									</a>
								</li>
								<li>
									<a href="user-profile">
										<img src="assets/img/icons/profile-icon.svg" alt="Icon">
										<span>Profile Setting</span>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- /Dashboard Menu -->

		<!-- Page Content -->
		<div class="content court-bg">
			<div class="container">

				<!-- Sort By -->
<!--				<div class="row">-->
<!--					<div class="col-lg-12">-->
<!--						<div class="sortby-section court-sortby-section">-->
<!--							<div class="sorting-info">-->
<!--								<div class="row d-flex align-items-center">-->
<!--									<div class="col-xl-7 col-lg-7 col-sm-12 col-12">-->
<!--										<div class="coach-court-list">-->
<!--											<ul class="nav">-->
<!--												<li><a class="active" href="user-bookings.html">Upcoming</a></li>-->
<!--												<li><a  href="user-complete.html">Completed</a></li>-->
<!--												<li><a  href="user-ongoing.html">On Going</a></li>-->
<!--												<li><a href="user-cancelled.html">Cancelled</a></li>-->
<!--											</ul>-->
<!--										</div>-->
<!--									</div>-->
<!--									<div class="col-xl-5 col-lg-5 col-sm-12 col-12">-->
<!--										<div class="sortby-filter-group court-sortby">-->
<!--											<div class="sortbyset week-bg">-->
<!--												<div class="sorting-select">-->
<!--													<select class="form-control select">-->
<!--														<option>This Week</option>-->
<!--														<option>One Day</option>-->
<!--													</select>-->
<!--												</div>-->
<!--											</div>-->
<!--											<div class="sortbyset">-->
<!--												<span class="sortbytitle">Sort By</span>-->
<!--												<div class="sorting-select">-->
<!--													<select class="form-control select">-->
<!--														<option>Relevance</option>-->
<!--														<option>Price</option>-->
<!--													</select>-->
<!--												</div>-->
<!--											</div>-->
<!--										</div>-->
<!--									</div>-->
<!--								</div>-->
<!---->
<!--							</div>-->
<!--						</div>-->
<!--					</div>-->
<!--				</div>-->
				<!-- Sort By -->


                <div class="row">
                    <div class="col-xl-7 col-lg-12 d-flex">
                        <div class="card dashboard-card flex-fill">
                            <div class="card-header card-header-info">
                                <div class="card-header-inner">
                                    <h4>My Bookings</h4>
                                </div>
                            </div>
                            <div class="tab-content">
                                <div class="tab-pane fade show active" id="nav-Court" role="tabpanel" aria-labelledby="nav-Court-tab"
                                     tabindex="0">
                                    <div class="table-responsive dashboard-table-responsive">
                                        <table class="table dashboard-card-table">
                                            <tbody>
                                            <tr>
                                                <td>
                                                    <div class="academy-info">
                                                        <a href="user-bookings.html" class="academy-img">
                                                                <img src="assets/img/venues/venues-01.jpg" alt="Booking">
                                                            </a>
                                                        <div class="academy-content">
                                                            <h6><a href="#" data-bs-toggle="modal" data-bs-target="#upcoming-court">REAL SOCCER</a></h6>
                                                            <ul>
                                                                <li>Guests : 4</li>
                                                                <li>2 Hrs</li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <h6>Date & Time</h6>
                                                    <p>Mon, Jul 11</p>
                                                    <p>06:00 PM - 08:00 PM</p>
                                                </td>
                                                <td>
                                                    <h4>35$</h4>
                                                </td>
                                                <td>
                                                    <div class="dropdown dropdown-action">
                                                        <a href="#" class="action-icon dropdown-toggle" data-bs-toggle="dropdown"
                                                           aria-expanded="false"><i class="fas fa-ellipsis"></i></a>
                                                        <div class="dropdown-menu dropdown-menu-end">
                                                            <a class="dropdown-item" href="javascript:void(0);">
                                                                <i class="feather-x-circle"></i> Cancel
                                                            </a>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div class="academy-info">
                                                        <a href="user-bookings.html" class="academy-img">
                                                            <img src="assets/img/venues/venues-03.jpg"" alt="Booking">
                                                        </a>
                                                        <div class="academy-content">
                                                            <h6><a data-bs-toggle="modal" data-bs-target="#upcoming-court">DOWNTOWN SPORT</a></h6>
                                                            <ul>
                                                                <li>Guests : 4</li>
                                                                <li>2 Hrs</li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <h6>Date & Time</h6>
                                                    <p>Wen, Jul 13</p>
                                                    <p>7:00PM - 9:00PM</p>
                                                </td>
                                                <td>
                                                    <h4>50$</h4>
                                                </td>
                                                <td>
                                                    <div class="dropdown dropdown-action">
                                                        <a href="#" class="action-icon dropdown-toggle" data-bs-toggle="dropdown"
                                                           aria-expanded="false"><i class="fas fa-ellipsis"></i></a>
                                                        <div class="dropdown-menu dropdown-menu-end">
                                                            <a class="dropdown-item" href="javascript:void(0);">
                                                                <i class="feather-x-circle"></i> Cancel
                                                            </a>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div class="academy-info">
                                                        <a href="user-bookings.html" class="academy-img">
                                                            <img src="assets/img/venues/venues-04.jpg"" alt="Booking">
                                                        </a>
                                                        <div class="academy-content">
                                                            <h6><a data-bs-toggle="modal" data-bs-target="#upcoming-court">Ministry Of National Defense Football Club</a></h6>
                                                            <ul>
                                                                <li>Guests : 5</li>
                                                                <li>2 Hrs</li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <h6>Date & Time</h6>
                                                    <p>Thu, Jul 14</p>
                                                    <p>5:00 AM - 7:00 AM</p>
                                                </td>
                                                <td>
                                                    <h4>35$</h4>
                                                </td>
                                                <td>
                                                    <div class="dropdown dropdown-action">
                                                        <a href="#" class="action-icon dropdown-toggle" data-bs-toggle="dropdown"
                                                           aria-expanded="false"><i class="fas fa-ellipsis"></i></a>
                                                        <div class="dropdown-menu dropdown-menu-end">
                                                            <a class="dropdown-item" href="javascript:void(0);">
                                                                <i class="feather-x-circle"></i> Cancel
                                                            </a>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="nav-Coaching" role="tabpanel" aria-labelledby="nav-Coaching-tab" tabindex="0">
                                    <div class="table-responsive dashboard-table-responsive">
                                        <table class="table dashboard-card-table">
                                            <tbody>
                                            <tr>
                                                <td>
                                                    <div class="academy-info">
                                                        <a href="user-bookings.html" class="academy-img">
                                                            <img src="assets/img/featured/featured-05.jpg" alt="Booking">
                                                        </a>
                                                        <div class="academy-content">
                                                            <h6 class="mb-1"><a data-bs-toggle="modal" data-bs-target="#upcoming-coach">Kevin Anderson</a></h6>
                                                            <span class="mb-0">Onetime</span>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <h6>Date & Time</h6>
                                                    <p>Mon, Jul 11</p>
                                                    <p>06:00 PM - 08:00 PM</p>
                                                </td>
                                                <td>
                                                    <h4>$400</h4>
                                                </td>
                                                <td>
                                                    <div class="dropdown dropdown-action">
                                                        <a href="#" class="action-icon dropdown-toggle" data-bs-toggle="dropdown"
                                                           aria-expanded="false"><i class="fas fa-ellipsis"></i></a>
                                                        <div class="dropdown-menu dropdown-menu-end">
                                                            <a class="dropdown-item" href="javascript:void(0);">
                                                                <i class="feather-x-circle"></i> Cancel
                                                            </a>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div class="academy-info">
                                                        <a href="user-bookings.html" class="academy-img">
                                                            <img src="assets/img/featured/featured-06.jpg" alt="Booking">
                                                        </a>
                                                        <div class="academy-content">
                                                            <h6 class="mb-1"> <a data-bs-toggle="modal" data-bs-target="#upcoming-coach">Angela Roudrigez</a></h6>
                                                            <span class="mb-0">Single Lesson</span>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <h6>Date & Time</h6>
                                                    <p>Tue, Jul 12</p>
                                                    <p>07:00 PM - 08:00 PM</p>
                                                </td>
                                                <td>
                                                    <h4>$240</h4>
                                                </td>
                                                <td>
                                                    <div class="dropdown dropdown-action">
                                                        <a href="#" class="action-icon dropdown-toggle" data-bs-toggle="dropdown"
                                                           aria-expanded="false"><i class="fas fa-ellipsis"></i></a>
                                                        <div class="dropdown-menu dropdown-menu-end">
                                                            <a class="dropdown-item" href="javascript:void(0);">
                                                                <i class="feather-x-circle"></i> Cancel
                                                            </a>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div class="academy-info">
                                                        <a href="user-bookings.html" class="academy-img">
                                                            <img src="assets/img/featured/featured-07.jpg" alt="Booking">
                                                        </a>
                                                        <div class="academy-content">
                                                            <h6 class="mb-1"> <a data-bs-toggle="modal" data-bs-target="#upcoming-coach">Evon Raddick</a></h6>
                                                            <span class="mb-0">Onetime</span>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <h6>Date & Time</h6>
                                                    <p>Wen, Jul 13</p>
                                                    <p>10:00 PM - 11:00 PM</p>
                                                </td>
                                                <td>
                                                    <h4>$320</h4>
                                                </td>
                                                <td>
                                                    <div class="dropdown dropdown-action">
                                                        <a href="#" class="action-icon dropdown-toggle" data-bs-toggle="dropdown"
                                                           aria-expanded="false"><i class="fas fa-ellipsis"></i></a>
                                                        <div class="dropdown-menu dropdown-menu-end">
                                                            <a class="dropdown-item" href="javascript:void(0);">
                                                                <i class="feather-x-circle"></i> Cancel
                                                            </a>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div class="academy-info">
                                                        <a href="user-bookings.html" class="academy-img">
                                                            <img src="assets/img/featured/featured-08.jpg" alt="Booking">
                                                        </a>
                                                        <div class="academy-content">
                                                            <h6 class="mb-1"> <a data-bs-toggle="modal" data-bs-target="#upcoming-coach">Harry Richardson</a></h6>
                                                            <span class="mb-0">Onetime</span>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <h6>Date & Time</h6>
                                                    <p>Thu, Jul 14</p>
                                                    <p>09:00 AM - 10:00 AM</p>
                                                </td>
                                                <td>
                                                    <h4>$710</h4>
                                                </td>
                                                <td>
                                                    <div class="dropdown dropdown-action">
                                                        <a href="#" class="action-icon dropdown-toggle" data-bs-toggle="dropdown"
                                                           aria-expanded="false"><i class="fas fa-ellipsis"></i></a>
                                                        <div class="dropdown-menu dropdown-menu-end">
                                                            <a class="dropdown-item" href="javascript:void(0);">
                                                                <i class="feather-x-circle"></i> Cancel
                                                            </a>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div class="academy-info">
                                                        <a href="user-bookings.html" class="academy-img">
                                                            <img src="assets/img/featured/featured-09.jpg" alt="Booking">
                                                        </a>
                                                        <div class="academy-content">
                                                            <h6 class="mb-1"> <a data-bs-toggle="modal" data-bs-target="#upcoming-coach">Pete Hill</a></h6>
                                                            <span class="mb-0">Onetime</span>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <h6>Date & Time</h6>
                                                    <p>Fri, Jul 15</p>
                                                    <p>11:00 AM - 12:00 PM</p>
                                                </td>
                                                <td>
                                                    <h4>$820</h4>
                                                </td>
                                                <td>
                                                    <div class="dropdown dropdown-action">
                                                        <a href="#" class="action-icon dropdown-toggle" data-bs-toggle="dropdown"
                                                           aria-expanded="false"><i class="fas fa-ellipsis"></i></a>
                                                        <div class="dropdown-menu dropdown-menu-end">
                                                            <a class="dropdown-item" href="javascript:void(0);">
                                                                <i class="feather-x-circle"></i> Cancel
                                                            </a>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
			</div>
		</div>
		<!-- /Page Content -->

@endsection
