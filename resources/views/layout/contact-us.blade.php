@extends('master')
@section('title')
MN Sports
@endsection
@section('body')

		<!-- Breadcrumb -->
		<div class="breadcrumb breadcrumb-list mb-0">
			<span class="primary-right-round"></span>
			<div class="container mt-lg-3">
				<h1 class="text-white  mt-lg-5">Contact US</h1>
				<ul>
					<li><a href="home">Home</a></li>
					<li>Contact US</li>
				</ul>
			</div>
		</div>
		<!-- /Breadcrumb -->

		<!-- Page Content -->
<div class="content blog-details contact-group">
    <div class="container">
        <h2 class="text-center mb-4">Contact Information</h2> <!-- Reduced mb-40 to mb-4 -->
        <div class="row mb-4"> <!-- Reduced mb-40 to mb-4 -->
            <div class="col-12 col-sm-12 col-md-6 col-lg-4">
                <div class="d-flex justify-content-start align-items-center details">
                    <div class="info">
                        <h4>Email Address</h4>
                        <p>somonysn20@gmail.com</p>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-12 col-md-6 col-lg-4">
                <div class="d-flex justify-content-start align-items-center details">
                    <div class="info">
                        <h4>Phone Number</h4>
                        <p>097 879 6615</p>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-12 col-md-6 col-lg-4">
                <div class="d-flex justify-content-start align-items-center details">
                    <div class="info">
                        <h4>Location</h4>
                        <p>Big stadium at DownTown Sport Strieng mean chhey</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
		<!-- /Page Content -->

@endsection
