@extends('master')
@section('title')
MN Sports
@endsection
@section('body')
<div class="breadcrumb mb-0">
    <span class="primary-right-round"></span>
    <div class="container mt-lg-3">
        <h1 class="text-white mt-lg-5">Booking</h1>
        <ul>
            <li><a href="home">Home</a></li>
            <li>Booking</li>
        </ul>
    </div>
</div>
<section class="booking-steps py-30">
    <div class="container">
        <ul class="d-lg-flex justify-content-center align-items-center">
            <li class="active"><h5><a href="cage-details"><span>1</span>Booking</a></h5></li>
        </ul>
    </div>
</section>
<div class="content book-cage">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-8">
                <section class="card booking-form">
                    <h3 class="border-bottom">Booking Form</h3>
                    <form>
                        <div class="mb-3">
                            <label for="date" class="form-label">From</label>
                            <div class="form-icon">
                                <input type="text" class="form-control datetimepicker" placeholder="Select Date" id="date">
                                <span class="cus-icon">
										<i class="feather-calendar icon-date"></i>
									</span>
                            </div>

                        </div>
                        <div class="mb-3">
                            <label for="start-time" class="form-label">Start Time</label>
                            <div class='form-icon'>
                                <input type="text" class="form-control timepicker" id="start-time" placeholder="Select Start Time">
                                <span class="cus-icon"><i class="feather-clock icon-time"></i></span>
                            </div>
                        </div>
                        <div class="mb-3">
                            <label for="end-time" class="form-label">End Time</label>
                            <div class='form-icon'>
                                <input type="text" class="form-control timepicker" id="end-time" placeholder="Select End Time">
                                <span class="cus-icon"><i class="feather-clock icon-time"></i></span>
                            </div>
                        </div>
                        <div class="mb-3">
                            <label for="court" class="form-label">Court</label>
                            <select class="select" id="court">
                                <option>Select City</option>
                                <option>Tornoto</option>
                                <option>Texas</option>
                            </select>
                        </div>
                        <div class="select-guest">
                            <h5>Select Guest</h5><span class="primary-text"> (2 guests maximum)</span>
                            <label class="w-100">The youngest age allowed at this court is 5 <span>*</span></label>
                            <div class="d-md-flex justify-content-between align-items-center">
                                <div class="qty-item text-center">
                                    <a href="javascript:void(0);" class="dec d-flex justify-content-center align-items-center"><i class="feather-minus-circle"></i></a>
                                    <input type="number" class="form-control text-center" name="qty" id="adults" value="1" >
                                    <a href="javascript:void(0);" class="inc d-flex justify-content-center align-items-center"><i class="feather-plus-circle"></i></a>
                                    <label for="adults">
                                        <span class="dark-text">Adults</span>
                                        <span class="dull-text">Ages 13 and up</span>
                                    </label>
                                </div>

                                <div class="qty-item text-center">
                                    <a href="javascript:void(0);" class="dec d-flex justify-content-center align-items-center"><i class="feather-minus-circle"></i></a>
                                    <input type="number" class="form-control text-center" name="qty" id="children" value="1" >
                                    <a href="javascript:void(0);" class="inc d-flex justify-content-center align-items-center"><i class="feather-plus-circle"></i></a>
                                    <label for="children">
                                        <span class="dark-text">Children</span>
                                        <span class="dull-text">Ages 6-12</span>
                                    </label>
                                </div>

                                <div class="qty-item text-center">
                                    <a href="javascript:void(0);" class="dec d-flex justify-content-center align-items-center"><i class="feather-minus-circle"></i></a>
                                    <input type="number" class="form-control text-center" name="qty" id="young-children" value="1" >
                                    <a href="javascript:void(0);" class="inc d-flex justify-content-center align-items-center"><i class="feather-plus-circle"></i></a>
                                    <label for="young-children">
                                        <span class="dark-text">Young Children</span>
                                        <span class="dull-text">Under 6</span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </form>
                </section>
                <div class="text-center mb-40">
                    <h3 class="mb-1 mt-lg-5">Personal Information</h3>
                    <p class="sub-title mb-0">Share your info and embark on a sporting journey.</p>
                </div>
                <div class="card">
                    <h3 class="border-bottom">Enter Details</h3>
                    <form>
                        <div class="mb-3">
                            <label for="name" class="form-label">Name</label>
                            <input type="text" class="form-control" id="name" placeholder="Enter Name">
                        </div>
                        <div class="mb-3">
                            <label for="email" class="form-label">Email</label>
                            <input type="email" class="form-control" id="email" placeholder="Enter Email Address">
                        </div>
                        <div class="mb-3">
                            <label for="name" class="form-label">Phone Number</label>
                            <input type="text" class="form-control" id="phonenumber" placeholder="Enter Phone Number">
                        </div>
                        <div class="mb-3">
                            <label for="name" class="form-label">Your Address</label>
                            <input type="text" class="form-control" id="address" placeholder="Enter Address">
                        </div>
                        <div>
                            <label for="comments" class="form-label">Details</label>
                            <textarea class="form-control" id="comments" rows="3" placeholder="Enter Comments"></textarea>
                        </div>
                    </form>
                </div>
            </div>

            <div class="col-12 col-sm-12 col-md-12 col-lg-4">
                <aside class="card booking-details">
                    <h3 class="border-bottom">Booking Details</h3>
                    <ul>
                        <li><i class="fa-regular fa-building me-2"></i>Standard Synthetic Court 1<span class="x-circle"></span></li>
                        <li><i class="feather-calendar me-2"></i>27, April 2023</li>
                        <li><i class="feather-clock me-2"></i>01:00 PM to 03:00 PM</li>
                        <li><i class="feather-users me-2"></i>2  Adults, 1 Children</li>
                    </ul>
                    <div class="d-grid btn-block">
                        <h1>Subtotal : $50</h1>
                    </div>
                </aside>
                <aside class="card payment-modes mt-lg-5">
                    <h3 class="border-bottom">Checkout</h3>
                    <h6 class="mb-3">Select Payment Gateway</h6>
                    <div class="radio">
                        <div class="form-check form-check-inline mb-3">
                            <input class="form-check-input default-check me-2" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="Credit Card">
                            <label class="form-check-label" for="inlineRadio1">Credit Card</label>
                        </div>
                        <div class="form-check form-check-inline mb-3">
                            <input class="form-check-input default-check me-2" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="Paypal">
                            <label class="form-check-label" for="inlineRadio2">Paypal</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input default-check me-2" type="radio" name="inlineRadioOptions" id="inlineRadio3" value="Wallet">
                            <label class="form-check-label" for="inlineRadio3">Wallet</label>
                        </div>
                    </div>
                    <hr>
                    <ul class="order-sub-total">
                        <li>
                            <p>Sub total</p>
                            <h6>50$</h6>
                        </li>
                        <li>
                            <p>Additional Guest</p>
                            <h6>Free</h6>
                        </li>
                        <li>
                            <p>Service charge</p>

                        </li>
                    </ul>
                    <div class="order-total d-flex justify-content-between align-items-center">
                        <h5>Order Total</h5>
                        <h5>50$</h5>
                    </div>
<!--                    <div class="form-check d-flex justify-content-start align-items-center policy">-->
<!--                        <div class="d-inline-block">-->
<!--                            <input class="form-check-input" type="checkbox" value="" id="policy">-->
<!--                        </div>-->
<!--                        <label class="form-check-label mt-3 mb-3" for="policy"><a href="privacy-policy.html">Privacy Policy</a> and <a href="terms-condition.html">Terms of Use</a></label>-->
<!--                    </div>-->
                    <div class="d-grid btn-block">
                        <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#bookingconfirmModal">Proceed  50$</button>
                    </div>
                </aside>
            </div>
        </div>
        <div class="text-center btn-row">
            <a class="btn btn-primary me-3 btn-icon" href="home"><i class="feather-arrow-left-circle me-1"></i> Back</a>
        </div>
    </div>
    <!-- /Container -->
</div>
<div class="modal fade" id="bookingconfirmModal" tabindex="-1" aria-labelledby="bookingconfirmModal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header text-center d-inline-block">
                <img src="assets/img/icons/booking-confirmed.svg" alt="User">
            </div>
            <div class="modal-body text-center">
                <h3 class="mb-3">Booking has been Confirmed</h3>
                <p>Check your email on the booking confirmation</p>
            </div>
            <div class="modal-footer text-center d-inline-block">
                <a href="user-dashboard" class="btn btn-primary btn-icon"><i class="feather-arrow-left-circle me-1"></i>Back to Dashboard</a>
            </div>
        </div>
    </div>
</div>
@endsection
