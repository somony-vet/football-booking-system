@extends('master')
@section('title')
MN Sports
@endsection
@section('body')

		<!-- Breadcrumb -->
		<div class="breadcrumb breadcrumb-list mb-0">
			<span class="primary-right-round"></span>
			<div class="container mt-lg-3">
				<h1 class="text-white mt-lg-5">About Us</h1>
				<ul>
					<li><a href="home">Home</a></li>
					<li>About Us</li>
				</ul>
			</div>
		</div>
		<!-- /Breadcrumb -->

		<!-- Page Content -->
		<div class="content">
			<!-- About Us Info -->
			<section class="aboutus-info">
				<div class="container">
					<!-- Banners -->
					<div class="row d-flex align-items-center">
						<div class=" col-12 col-sm-3 col-md-3 col-lg-3">
							<div class="banner text-center">
								<img src="assets/img/aboutus/banner-01.jpg" class="img-fluid corner-radius-10" alt="Banner-01">
							</div>
						</div>
						<div class=" col-12 col-sm-6 col-md-6 col-lg-6">
							<div class="banner text-center">
								<img src="assets/img/aboutus/banner-02.jpg" class="img-fluid corner-radius-10" alt="Banner-02">
							</div>
						</div>
						<div class=" col-12 col-sm-3 col-md-3 col-lg-3">
							<div class="banner text-center">
								<img src="assets/img/aboutus/banner-03.jpg" class="img-fluid corner-radius-10" alt="Banner-03">
							</div>
						</div>
					</div>
					<!-- /Banners -->

					<!-- Vision-Mission -->
					<div class="vision-mission">
						<div class="row">
							<div class=" col-12 col-sm-12 col-md-12 col-lg-8">
								<h2>Football</h2>
								<p>Sport covers a range of activities performed within a set of rules and undertaken as part of leisure or competition. Sporting activities involve physical activity carried out by teams or individuals and may be supported by an institutional framework, such as a sporting agency.</p>
							</div>
							<div class=" col-12 col-sm-12 col-md-12 col-lg-4">
								<div class="mission-bg">
									<h2>Our Mission</h2>
									<p>We provide coaches and players with a seamless platform for connectivity, personalized insights, and educational resources. Together, we foster a collaborative community that supports growth and success in football..</p>
								</div>
							</div>
						</div>
					</div>
					<!-- /Vision-Mission -->
				</div>
			</section>
			<!-- /About Us Info -->


			<!-- Group Coaching -->
			<!-- Group Coaching -->

			<!-- Testimonials -->
			<!-- /Testimonials -->

			<!-- Featured Plans -->
			s
			<!-- /Featured Plans -->

			<!-- Latest News -->
			<!-- /Latest News -->

			<!-- Newsletter -->
			<section class="section newsletter-sport">
				<div class="container">
					<div class="row">
						<div class="col-sm-12">
							<div class="subscribe-style">
								<div class="banner-blk">
									<img src="assets/img/subscribe-bg.jpg" class="img-fluid" alt="Banner">
								</div>
								<div class="banner-info ">
									<img src="assets/img/icons/subscribe.svg" class="img-fluid" alt="Banner">
									<h2>Subscribe to Newsletter</h2>
									<p>Just for you, exciting badminton news updates.</p>
									<div class="subscribe-blk bg-white">
										<div class="input-group align-items-center">
											<i class="feather-mail"></i>
											<input type="email" class="form-control" placeholder="Enter Email Address" aria-label="email">
											<div class="subscribe-btn-grp">
												<input type="submit" class="btn btn-secondary" value="Subscribe">
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<!-- /Newsletter -->

		</div>
		<!-- /Page Content -->

@endsection
