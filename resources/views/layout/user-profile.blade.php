@extends('master')
@section('title')
MN Sports
@endsection
@section('body')
		<!-- Breadcrumb -->
		<section class="breadcrumb breadcrumb-list mb-0">
			<span class="primary-right-round"></span>
			<div class="container mt-lg-3">
				<h1 class="text-white">User Profile</h1>
				<ul>
					<li><a href="home">Home</a></li>
					<li >User Profile</li>
				</ul>
			</div>
		</section>
		<!-- /Breadcrumb -->

		<!-- Dashboard Menu -->
		<div class="dashboard-section">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="dashboard-menu">
							<ul>
								<li>
									<a href="user-dashboard">
										<img src="assets/img/icons/dashboard-icon.svg" alt="Icon">
										<span>Dashboard</span>
									</a>
								</li>
								<li>
									<a href="user-bookings" >
										<img src="assets/img/icons/booking-icon.svg" alt="Icon">
										<span>My Bookings</span>
									</a>
								</li>
								<li>
									<a href="user-chat">
										<img src="assets/img/icons/chat-icon.svg" alt="Icon">
										<span>Chat</span>
									</a>
								</li>
								<li>
									<a href="user-invoice" >
										<img src="assets/img/icons/invoice-icon.svg" alt="Icon">
										<span>Invoices</span>
									</a>
								</li>
								<li>
									<a href="user-wallet" >
										<img src="assets/img/icons/wallet-icon.svg" alt="Icon">
										<span>Wallet</span>
									</a>
								</li>
								<li>
									<a href="user-profile" class="active">
										<img src="assets/img/icons/profile-icon.svg" alt="Icon">
										<span>Profile Setting</span>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- /Dashboard Menu -->

		<!-- Page Content -->
		<div class="content court-bg">
			<div class="container">

				<div class="coach-court-list profile-court-list">
					<ul class="nav">
						<li><a class="active" href="user-profile">Profile</a></li>
						<li><a href="user-profile-othersetting">Other Settings</a></li>
					</ul>
				</div>

				<div class="row">
					<div class="col-sm-12">
						<div class="profile-detail-group">
							<div class="card ">
								<form >
									<div class="row">
										<div class="col-md-12">
											<div class="file-upload-text">
												<div class="file-upload">
													<img src="assets/img/icons/img-icon.svg" class="img-fluid" alt="Upload">
													<p>Upload Photo</p>
													<span>
														<i class="feather-edit-3"></i>
														<input type="file"  id="file-input">
													</span>
												</div>
												<h5>Upload a logo with a minimum size of 150 * 150 pixels (JPG, PNG, SVG).</h5>
											</div>
										</div>
										<div class="col-lg-4 col-md-6">
											<div class="input-space">
												<label  class="form-label">Name</label>
												<input type="text" class="form-control" id="name" placeholder="Enter Name">
											</div>
										</div>
										<div class="col-lg-4 col-md-6">
											<div class="input-space">
												<label  class="form-label">Email</label>
												<input type="email" class="form-control" id="email" placeholder="Enter Email Address">
											</div>
										</div>
										<div class="col-lg-4 col-md-6">
											<div class="input-space">
												<label  class="form-label">Phone Number</label>
												<input type="text" class="form-control" id="phone" placeholder="Enter Phone Number">
											</div>
										</div>
										<div class="col-lg-12 col-md-12">
											<div class="info-about">
												<label for="comments" class="form-label">Information about You</label>
												<textarea class="form-control" id="comments" rows="3" placeholder="About"></textarea>
											</div>
										</div>
										<div class="address-form-head">
											<h4>Address</h4>
										</div>
										<div class="col-lg-12 col-md-12">
											<div class="input-space">
												<label  class="form-label">Address</label>
												<input type="text" class="form-control" id="address" placeholder="Enter Address">
											</div>
										</div>
										<div class="col-lg-4 col-md-6">
											<div class="input-space">
												<label  class="form-label">State</label>
												<input type="text" class="form-control" id="state" placeholder="Enter State">
											</div>
										</div>
										<div class="col-lg-4 col-md-6">
											<div class="input-space">
												<label  class="form-label">City</label>
												<input type="text" class="form-control" id="city" placeholder="Enter City">
											</div>
										</div>
										<div class="col-lg-4 col-md-4">
											<div class="input-space">
												<label  class="form-label">Country</label>
												<select class="select">
													<option>Country</option>
													<option>Texas</option>
												</select>
											</div>
										</div>
										<div class="col-lg-4 col-md-6">
											<div class="input-space mb-0">
												<label  class="form-label">Zipcode</label>
												<input type="text" class="form-control" id="zipcode" placeholder="Enter Zipcode">
											</div>
										</div>
									</div>
								</form>
							</div>
							<div class="save-changes text-end">
								<a href="javascript:;" class="btn btn-primary reset-profile">Reset</a>
								<a href="user-profile-othersetting" class="btn btn-secondary save-profile">Next</a>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
		<!-- /Page Content -->

@endsection
