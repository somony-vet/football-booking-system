
@extends('master')
@section('title')
MN Sports
@endsection
@section('body')
		<!-- Breadcrumb -->
		<section class="breadcrumb breadcrumb-list mb-0">
			<span class="primary-right-round"></span>
			<div class="container mt-lg-3">
				<h1 class="text-white mt-lg-5">Wallet</h1>
				<ul>
					<li><a href="home">Home</a></li>
					<li >Wallet</li>
				</ul>
			</div>
		</section>
		<!-- /Breadcrumb -->

		<!-- Dashboard Menu -->
		<div class="dashboard-section">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="dashboard-menu">
							<ul>
								<li>
									<a href="user-dashboard">
										<img src="assets/img/icons/dashboard-icon.svg" alt="Icon">
										<span>Dashboard</span>
									</a>
								</li>
								<li>
									<a href="user-bookings" >
										<img src="assets/img/icons/booking-icon.svg" alt="Icon">
										<span>My Bookings</span>
									</a>
								</li>
								<li>
									<a href="user-chat">
										<img src="assets/img/icons/chat-icon.svg" alt="Icon">
										<span>Chat</span>
									</a>
								</li>
								<li>
									<a href="user-invoice" >
										<img src="assets/img/icons/invoice-icon.svg" alt="Icon">
										<span>Invoices</span>
									</a>
								</li>
								<li>
									<a href="user-wallet" class="active">
										<img src="assets/img/icons/wallet-icon.svg" alt="Icon">
										<span>Wallet</span>
									</a>
								</li>
								<li>
									<a href="user-profile">
										<img src="assets/img/icons/profile-icon.svg" alt="Icon">
										<span>Profile Setting</span>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- /Dashboard Menu -->

		<!-- Page Content -->
		<div class="content court-bg">
			<div class="container">

				<div class="row">

					<!-- Wallet Balance -->
					<div class="col-md-12 col-lg-5 d-flex">
						<div class="wallet-wrap flex-fill">
							<div class="wallet-bal">
								<div class="wallet-img">
									<div class="wallet-amt">
										<h5>Your Wallet Balance</h5>
										<h2>$4,544</h2>
									</div>
								</div>
								<div class="payment-btn">
									<a href="#" class="btn balance-add" data-bs-toggle="modal" data-bs-target="#add-payment">Add Payment</a>
								</div>
							</div>
							<ul>
								<li>
									<h6>Total Credit</h6>
									<h3>$350.40</h3>
								</li>
								<li>
									<h6>Total Debit</h6>
									<h3>$50.40</h3>
								</li>
								<li>
									<h6>Total transaction</h6>
									<h3>$480.40</h3>
								</li>
							</ul>
						</div>
					</div>
					<!-- /Wallet Balance -->

					<!-- Wallet Card -->
					<div class="col-md-12 col-lg-7 d-flex">
						<div class="your-card">
							<div class="your-card-head">
								<h3>Your Cards</h3>
								<a href="javascript:;" class="btn btn-secondary d-inline-flex align-items-center"  data-bs-toggle="modal" data-bs-target="#add-new-card">Add New Card</a>
							</div>
							<div class="row">
								<div class="col-lg-6 col-md-12">
									<div class="debit-card-blk">
										<div class="debit-card-balence">
											<span>Debit card</span>
											<h5>Balance in card : 1,234</h5>
											<div class="card-number">
												<h4>123145546655</h4>
											</div>
										</div>
										<div class="debit-card-img">
											<img src="assets/img/icons/visa-icon.svg" alt="Icon">
										</div>
									</div>
								</div>
								<div class="col-lg-6 col-md-12">
									<div class="debit-card-blk">
										<div class="debit-card-balence">
											<span>Debit card</span>
											<h5>Balance in card : 1,234</h5>
											<div class="card-number">
												<h4>314555884554</h4>
											</div>
										</div>
										<div class="debit-card-img">
											<img src="assets/img/icons/master-card.svg" alt="Icon">
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- /Wallet Card -->

				</div>


			</div>
		</div>
		<!-- /Page Content -->

@endsection
