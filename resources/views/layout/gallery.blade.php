@extends('master')
@section('title')
MN Sports
@endsection
@section('body')
		<!-- Breadcrumb -->
		<div class="breadcrumb breadcrumb-list mb-0">
			<span class="primary-right-round"></span>
			<div class="container mt-lg-3">
				<h1 class="text-white">Gallery</h1>
				<ul>
					<li><a href="home">Home</a></li>
					<li>Gallery</li>
				</ul>
			</div>
		</div>
		<!-- /Breadcrumb -->

		<!-- Page Content -->
		<div class="content gallery-blk">
			<div class="container">
				<div class="grid">
					<div class="grid-sizer"></div>
  					<div class="gutter-sizer"></div>
					<div class="grid-item gallery-widget-item">
						<a href="assets/img/gallery/gallery4/gallery-01.png" data-fancybox="gallery4">
							<img src="assets/img/gallery/gallery4/gallery-01.png" class="img-fluid" alt="Gallery">
						</a>
					</div>
					<div class="grid-item gallery-widget-item">
						<a href="assets/img/gallery/gallery4/gallery-02.png" data-fancybox="gallery4">
							<img src="assets/img/gallery/gallery4/gallery-02.png" class="img-fluid" alt="Gallery">
						</a>
					</div>
					<div class="grid-item gallery-widget-item">
						<a href="assets/img/gallery/gallery4/gallery-03.png" data-fancybox="gallery4">
							<img src="assets/img/gallery/gallery4/gallery-03.png" class="img-fluid" alt="Gallery">
						</a>
					</div>
					<div class="grid-item gallery-widget-item">
						<a href="assets/img/gallery/gallery4/gallery-04.png" data-fancybox="gallery4">
							<img src="assets/img/gallery/gallery4/gallery-04.png" class="img-fluid" alt="Gallery">
						</a>
					</div>
					<div class="grid-item">
						<a href="assets/img/gallery/gallery4/gallery-05.png" data-fancybox="gallery4">
							<img src="assets/img/gallery/gallery4/gallery-05.png" class="img-fluid" alt="gallery-05">
						</a>
					</div>
					<div class="grid-item gallery-widget-item">
						<a href="assets/img/gallery/gallery4/gallery-06.png" data-fancybox="gallery4">
							<img src="assets/img/gallery/gallery4/gallery-06.png" class="img-fluid" alt="Gallery">
						</a>
					</div>


				</div>
			</div>
		</div>
		<!-- /Page Content -->
@endsection
