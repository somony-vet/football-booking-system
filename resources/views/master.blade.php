<!DOCTYPE html>
<html lang="en">
<!-- Mirrored from dreamsports.dreamstechnologies.com/html/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 25 Jan 2024 15:38:03 GMT -->
<head>
    <meta charset="utf-8"/>
    <meta
        name="viewport"
        content="width=device-width, initial-scale=1.0, user-scalable=0"
    />
    <title>@yield('title')</title>

    <!-- Meta Tags -->
    <meta
        name="twitter:description"
        content="Elevate your badminton business with Dream Sports template. Empower coaches & players, optimize court performance and unlock industry-leading success for your brand."
    />
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>

    <meta
        name="description"
        content="Elevate your badminton business with Dream Sports template. Empower coaches & players, optimize court performance and unlock industry-leading success for your brand."
    />
    <meta
        name="keywords"
        content="badminton, coaching, event, players, training, courts, tournament, athletes, courts rent, lessons, court booking, stores, sports faqs, leagues, chat, wallet, invoice"
    />
    <meta name="author" content="Dreamguys - DreamSports"/>

    <meta name="twitter:card" content="summary_large_image"/>
    <meta name="twitter:site" content="@dreamguystech"/>
    <meta
        name="twitter:title"
        content="DreamSports -  Booking Coaches, Venue for tournaments, Court Rental template"
    />

    <meta name="twitter:image" content="assets/img/meta-image.jpg"/>
    <meta name="twitter:image:alt" content="DreamSports"/>

    <meta property="og:url" content="https://dreamsports.dreamguystech.com/"/>
    <meta
        property="og:title"
        content="DreamSports -  Booking Coaches, Venue for tournaments, Court Rental template"
    />
    <meta
        property="og:description"
        content="Elevate your badminton business with Dream Sports template. Empower coaches & players, optimize court performance and unlock industry-leading success for your brand."
    />
    <meta property="og:image" content="../assets/img/meta-image.jpg"/>
    <meta property="og:image:secure_url" content="assets/img/meta-image.jpg"/>
    <meta property="og:image:type" content="image/png"/>
    <meta property="og:image:width" content="1200"/>
    <meta property="og:image:height" content="600"/>

    <link
        rel="shortcut icon"
        type="image/x-icon"
        href="assets/img/favicon.png"
    />
    <link
        rel="apple-touch-icon"
        sizes="120x120"
        href="assets/img/apple-touch-icon-120x120.png"
    />
    <link
        rel="apple-touch-icon"
        sizes="152x152"
        href="assets/img/apple-touch-icon-152x152.png"
    />

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css"/>

    <!-- Owl Carousel CSS -->
    <link
        rel="stylesheet"
        href="assets/plugins/owl-carousel/owl.carousel.min.css"
    />
    <link
        rel="stylesheet"
        href="assets/plugins/owl-carousel/owl.theme.default.min.css"
    />

    <!-- Aos CSS -->
    <link rel="stylesheet" href="assets/plugins/aos/aos.css"/>

    <!-- Fontawesome CSS -->
    <link
        rel="stylesheet"
        href="assets/plugins/fontawesome/css/fontawesome.min.css"
    />
    <link rel="stylesheet" href="assets/plugins/fontawesome/css/all.min.css"/>

    <!-- Select CSS -->
    <link rel="stylesheet" href="assets/plugins/select2/css/select2.min.css"/>

    <!-- Feathericon CSS -->
    <link rel="stylesheet" href="assets/css/feather.css"/>

    <!-- Main CSS -->
    <link rel="stylesheet" href="assets/css/style.css"/>
</head>

<body>
<div id="global-loader">
    <div class="loader-img">
        <img src="assets/img/loader.png" class="img-fluid" alt="Global"/>
    </div>
</div>

<!-- Main Wrapper -->
<div class="main-wrapper">
    <!-- Header -->
    <header class="header header-trans">
        <div class="container-fluid">
            <nav class="navbar navbar-expand-lg header-nav">
                <div class="navbar-header">
                    <a id="mobile_btn" href="javascript:void(0);">
                <span class="bar-icon">
                  <span></span>
                  <span></span>
                  <span></span>
                </span>
                    </a>
                    <a href="/" class="navbar-brand logo">
                        <img src="assets/img/logo.svg" class="img-fluid" alt="Logo"/>
                    </a>
                </div>
                <div class="main-menu-wrapper">
                    <div class="menu-header">
                        <a href="/" class="menu-logo">
                            <img
                                src="assets/img/logo-black.svg"
                                class="img-fluid"
                                alt="Logo"
                            />
                        </a>
                        <a
                            id="menu_close"
                            class="menu-close"
                            href="javascript:void(0);"
                        >
                            <i class="fas fa-times"></i
                            ></a>
                    </div>
                    <ul class="main-nav">
                        <li><a href="/">Home</a></li>
                        <li class="has-submenu">
                            <a href="cage-details">Booking </a>
                            <ul class="submenu">

                            </ul>
                        </li>
                        <li class="has-submenu">
                            <a href="#">User <i class="fas fa-chevron-down"></i></a>
                            <ul class="submenu">
                                <li><a href="user-dashboard">User Dashboard</a></li>
                                <li><a href="user-bookings">Bookings</a></li>
                                <li><a href="user-chat">Chat</a></li>
                                <li><a href="user-invoice">Invoice</a></li>
                                <li><a href="user-wallet">Wallet</a></li>
                                <li><a href="user-profile">Profile Edit</a></li>
                                <li><a href="user-profile-othersetting">Other Settings</a></li>
                            </ul>

                        </li>
                        <li class="has-submenu">
                            <a href="#">Pages <i class="fas fa-chevron-down"></i></a>
                            <ul class="submenu">
                                <li><a href="about-us">About Us</a></li>
                                <li><a href="services">Services</a></li>
                                <li><a href="gallery">Gallery</a></li>
                            </ul>
                        </li>
                        <li class="has-submenu">
                            <a href="#">Blog <i class="fas fa-chevron-down"></i></a>
                            <ul class="submenu">
                                <li><a href="blog-list">Blog List</a></li>
                            </ul>
                        </li>
                        <li><a href="contact-us">Contact Us</a></li>
                    </ul>
                </div>
                {{-- <ul class="nav header-navbar-rht">
                    <li class="nav-item">
                        <div class="nav-link btn btn-white log-register">
                            <a href="login.html"
                            ><span><i class="feather-users"></i></span>Login</a
                            >
                            / <a href="register.html">Register</a>
                        </div>
                    </li>
                </ul> --}}
            </nav>
        </div>
    </header>
    <!-- /Header -->
    @yield('body')
    <!-- /Newsletter -->
    <!-- Footer -->
    <footer class="footer">
        <div class="container">
            <!-- Footer Join -->
            <div class="footer-join aos" data-aos="fade-up">
                <h2>We are welcome.</h2>


            </div>
            <!-- /Footer Join -->

            <!-- Footer Top -->
            <div class="footer-top">
                <div class="row">
                    <div class="col-lg-2 col-md-6">
                        <!-- Footer Widget -->
                        <div class="footer-widget footer-menu">
                            <h4 class="footer-title">Contact us</h4>
                            <div class="footer-address-blk">
                                <div class="footer-call">
                                    <p>097 879 6615</p>
                                </div>
                                <div class="footer-call">
                                    <span>Need Live Suppot</span>
                                    <p>
                                        <a
                                            href="https://dreamsports.dreamstechnologies.com/cdn-cgi/l/email-protection"
                                            class="__cf_email__"
                                            data-cfemail="4e2a3c2b2f233d3e213c3a3d0e2b362f233e222b602d2123"
                                        >[email&#160;protected]</a
                                        >
                                    </p>
                                </div>
                            </div>

                        </div>
                        <!-- /Footer Widget -->
                    </div>
                    <div class="col-lg-2 col-md-6">
                        <!-- Footer Widget -->
                        <div class="footer-widget footer-menu">
                            <h4 class="footer-title">Quick Links</h4>
                            <ul>
                                <li>
                                    <a href="services">Services</a>
                                </li>
                                <li>
                                    <a href="blog-list">Blogs List</a>
                                </li>
                                <li>
                                    <a href="contact-us">Contact us</a>
                                </li>
                            </ul>
                        </div>
                        <!-- /Footer Widget -->
                    </div>
                    <div class="col-lg-2 col-md-6">
                        <!-- Footer Widget -->
                        <div class="footer-widget footer-menu">
                            <h4 class="footer-title">Support</h4>
                            <ul>
                                <li>
                                    <a href="contact-us">Contact Us</a>
                                </li>
                            </ul>
                        </div>
                        <!-- /Footer Widget -->
                    </div>
                    <div class="col-lg-2 col-md-6">
                        <!-- Footer Widget -->
                        <div class="footer-widget footer-menu">
                            <h4 class="footer-title">Other Links</h4>
                            <ul>
                                <li>
                                    <a href="cage-details">Booking</a>
                                </li>
                                <li>
                                    <a href="user-dashboard">User Dashboard</a>
                                </li>
                                <li>
                                    <a href="user-invoice">invoice</a>
                                </li>
                            </ul>
                        </div>
                        <!-- /Footer Widget -->
                    </div>
                    <div class="col-lg-2 col-md-6">
                        <!-- Footer Widget -->
                        <div class="footer-widget footer-menu">
                            <h4 class="footer-title">Our Locations</h4>
                            <ul>
                                <li>
                                    <a href="javascript:void(0);">Toul Kork</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);">DownTown</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);">T-soccer</a>
                                </li>
                            </ul>
                        </div>
                        <!-- /Footer Widget -->
                    </div>
                    <div class="col-lg-2 col-md-6">
                        <!-- Footer Widget -->
                        <div class="footer-widget footer-menu">
                            <h4 class="footer-title">Download</h4>
                            <div class="social-icon ">
                                <ul class="mb-lg-3 ">
                                    <li>
                                        <a href="javascript:void(0);" class="facebook"
                                        ><i class="fab fa-facebook-f"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);" class="twitter"
                                        ><i class="fab fa-twitter"></i>
                                        </a>
                                    </li>
                                </ul>
                                <ul>
                                    <li>
                                        <a href="javascript:void(0);" class="instagram"
                                        ><i class="fab fa-instagram"></i
                                            ></a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);" class="linked-in"
                                        ><i class="fab fa-linkedin-in"></i
                                            ></a>
                                    </li>
                                </ul>

                            </div>
                        </div>
                        <!-- /Footer Widget -->
                    </div>
                </div>
            </div>
            <!-- /Footer Top -->
        </div>

        <!-- Footer Bottom -->
        <div class="footer-bottom">
            <div class="container">
                <!-- Copyright -->
                <div class="copyright">
                    <div class="row align-items-center">
                        <div class="col-md-6">
                            <div class="copyright-text">
                                <p class="mb-0">
                                    &copy; 2024 MN Sports - All rights reserved.
                                </p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <!-- Copyright Menu -->
                            <div class="dropdown-blk">
                                <ul class="navbar-nav selection-list">
                                    <li class="nav-item dropdown">
                                        <div class="lang-select">
                          <span class="select-icon"
                          ><i class="feather-globe"></i
                              ></span>
                                            <select class="select">
                                                <option>Khmer</option>
                                                <option>English (US)</option>
                                            </select>
                                        </div>
                                    </li>
                                    <li class="nav-item dropdown">
                                        <div class="lang-select">
                                            <span class="select-icon"></span>
                                            <select class="select">
                                                <option>រៀល</option>
                                                <option>$ USD</option>
                                            </select>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <!-- /Copyright Menu -->
                        </div>
                    </div>
                </div>
                <!-- /Copyright -->
            </div>
        </div>
        <!-- /Footer Bottom -->
    </footer>
    <!-- /Footer -->
</div>
<!-- /Main Wrapper -->

<!-- scrollToTop start -->
<div class="progress-wrap active-progress">
    <svg
        class="progress-circle svg-content"
        width="100%"
        height="100%"
        viewBox="-1 -1 102 102"
    >
        <path
            d="M50,1 a49,49 0 0,1 0,98 a49,49 0 0,1 0,-98"
            style="
            transition: stroke-dashoffset 10ms linear 0s;
            stroke-dasharray: 307.919px, 307.919px;
            stroke-dashoffset: 228.265px;
          "
        ></path>
    </svg>
</div>
<!-- scrollToTop end -->

<!-- jQuery -->
<script
    data-cfasync="false"
    src="../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"
></script>
<script src="assets/js/jquery-3.7.0.min.js"></script>

<!-- Bootstrap Core JS -->
<script src="assets/js/bootstrap.bundle.min.js"></script>

<!-- Select JS -->
<script src="assets/plugins/select2/js/select2.min.js"></script>

<!-- Owl Carousel JS -->
<script src="assets/plugins/owl-carousel/owl.carousel.min.js"></script>

<!-- Aos -->
<script src="assets/plugins/aos/aos.js"></script>

<!-- Counterup JS -->
<script src="assets/js/jquery.waypoints.js"></script>
<script src="assets/js/jquery.counterup.min.js"></script>

<!-- Top JS -->
<script src="assets/js/backToTop.js"></script>

<!-- Custom JS -->
<script src="assets/js/script.js"></script>
</body>

<!-- Mirrored from dreamsports.dreamstechnologies.com/html/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 25 Jan 2024 15:38:40 GMT -->
</html>
